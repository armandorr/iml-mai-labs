"""
File that defines the different voting schemes available
"""

import numpy as np

def majorityClass(k, distancesSamples, labels):
    """
        Args:
            - k (int): number of nearest neighbors to consider
            - distancesSamples: distances between each test sample with all the train instances
            - labels: labels of the train instances
    """

    # Sort distances
    sortedDistancesIndexes = np.argsort(distancesSamples, axis=1).astype(int)

    # Assign to each test sample the majority class from the K nearest train neighbors
    allPredLabels = []
    for idx, closestNeighborsIndexes in enumerate(sortedDistancesIndexes[:, :k]):

        # Train nearest neighbors labels
        closestNeighborsLabels = labels[closestNeighborsIndexes]
        unique, counts = np.unique(closestNeighborsLabels, return_counts=True)

        # Majority class (sorted classes)
        predLabels_aux = unique[np.argsort(1 / counts)]
        predLabel = predLabels_aux[0]

        # If equal votes, get the closest one. If there is a tie, choose random
        if len(counts) != 1 and counts[0] == counts[1]:
            distances = distancesSamples[idx][closestNeighborsIndexes]
            i = 1
            while i < len(counts) and distances[i] == distances[i - 1]:
                predLabel = np.random.choice(predLabels_aux[0:i + 1], 1)[0]
                i += 1

        allPredLabels.append(predLabel)

    return allPredLabels


def _inverseDistanceWeighted_votes(distances, allLabels, labelC):

    values = np.zeros(len(allLabels))
    num = ((allLabels == labelC).astype(int))
    values[distances != 0] = num[distances != 0] / distances[distances != 0]

    # return np.sum((allLabels == labelC).astype(int) / distances)
    return np.sum(values)

def _sheppardWork_votes(distances, allLabels, labelC):
    return np.sum(np.exp(-distances) * (allLabels == labelC).astype(int))

def distanceWeightedMethods(k, distancesSamples, labels, methodType="inverseDistance"):
    """
        Args:
            - k (int): number of nearest neighbors to consider
            - distancesSamples: distances between each test sample with all the train instances
            - labels: labels of the train instances
            - methodType (str): name of the method to use
                    - inverseDistance: assign more weight to nearer neighbors
                    - sheppardWork: use exponential function
    """

    assert methodType in ["inverseDistance",
                          "sheppardWork"], "Please define a correct method: inverseDistance or sheppardWork"

    # Possible classes
    labels_unique = np.unique(labels)

    # Sort distances
    sortedDistancesIndexes = np.argsort(distancesSamples, axis=1).astype(int)

    # Assign to each test sample the majority class from the K nearest train neighbors
    allPredLabels = []
    for j, a in enumerate(sortedDistancesIndexes[:, :k]):

        # Distances to the train nearest neighbors
        distances = distancesSamples[j][a]
        labels_aux = labels[a]

        # Votes to each label (according to the method)
        results = []
        for i, label_unique in enumerate(labels_unique):
            if methodType == "inverseDistance":
                votes = _inverseDistanceWeighted_votes(distances, labels_aux, label_unique)
            else:  # "sheppardWork"
                votes = _sheppardWork_votes(distances, labels_aux, label_unique)
            results.append(votes)

        # Label highest vote
        predLabelIndex = np.argmax(results)
        predLabel = labels_unique[predLabelIndex]

        # Sort votes (search for equal values)
        results = np.array(results)
        results_indexes = np.argsort(-results).astype(int)
        results = results[results_indexes]

        # If equal votes, get the closest one. If there is a tie, choose random
        i = 0
        while i < len(results) and results[i] == results[i - 1]:
            predLabelIndex = np.random.choice(results_indexes[0:i + 1], 1)[0]
            predLabel = labels_unique[predLabelIndex]
            i += 1

        allPredLabels.append(predLabel)

    return allPredLabels
