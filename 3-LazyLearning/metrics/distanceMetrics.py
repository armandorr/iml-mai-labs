"""
File that defines the different metrics available
"""

import numpy as np
import pandas as pd


def minkowski(p, train, test, weights=None):
    """
        Args:
            - p (int): power of the Minkowski formula (p=1: Manhattan, p=2: Euclidean)
            - train: Training data instances.
            - test: Testing data instances.
            - weights: Weights of each feature of the instances
    """

    # Train and test samples
    datatrain = train.copy()
    datatest = test.copy()

    if isinstance(datatrain, pd.Series): datatrain = datatrain.to_frame(name="train").T
    if isinstance(datatest, pd.Series): datatest = datatest.to_frame(name="test").T

    # If distances not for the KNN algorithm (not weights)
    if weights is None:
        if len(test.shape) <= 1:
            shape = test.shape[0]
        else:
            shape = test.shape[1]
        weights = np.ones(shape)

    # Numerical columns and values
    numericalColumns = datatrain.select_dtypes('number')
    numericalColumnsIdx = [datatrain.columns.get_loc(c) for c in numericalColumns.columns]
    datatrainNumerical = datatrain.loc[:, numericalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestNumerical = datatest[:, numericalColumnsIdx]
    else:
        datatestNumerical = datatest.loc[:, numericalColumns.columns].values

    # Categorical columns and values
    categoricalColumns = datatrain.select_dtypes('object')
    categoricalColumnsIdx = [datatrain.columns.get_loc(c) for c in categoricalColumns.columns]
    datatrainCategorical = datatrain.loc[:, categoricalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestCategorical = datatest[:, categoricalColumnsIdx]
    else:
        datatestCategorical = datatest.loc[:, categoricalColumns.columns].values

    # Compare each test sample with all train instances
    allDistances = np.zeros((test.shape[0], train.shape[0]))
    for i, (testSampleNumerical, testSampleCategorical) in enumerate(zip(datatestNumerical, datatestCategorical)):

        # Numerical data
        diff = (np.subtract(datatrainNumerical, testSampleNumerical)) ** p
        diffNumerical = np.sum(weights[numericalColumnsIdx] * diff, axis=1)

        # Categorical data
        diff = np.uint8(datatrainCategorical != testSampleCategorical)
        diffCategorical = np.sum(weights[categoricalColumnsIdx] * diff, axis=1)

        # Distances
        distancestrainSampleTest = (diffNumerical + diffCategorical) ** (1 / p)
        allDistances[i, :] = distancestrainSampleTest

    return allDistances


def cosine(train, test, weights):
    """
        Args:
            - train: Training data instances.
            - test: Testing data instances.
            - weights: Weights of each feature of the instances
    """

    # Train and test samples
    datatrain = train.copy()
    datatest = test.copy()

    # Numerical columns and values
    numericalColumns = datatrain.select_dtypes('number')
    numericalColumnsIdx = [datatrain.columns.get_loc(c) for c in numericalColumns.columns]
    datatrainNumerical = datatrain.loc[:, numericalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestNumerical = datatest[:, numericalColumnsIdx]
    else:
        datatestNumerical = datatest.loc[:, numericalColumns.columns].values

    # Categorical columns and values
    categoricalColumns = datatrain.select_dtypes('object')
    categoricalColumnsIdx = [datatrain.columns.get_loc(c) for c in categoricalColumns.columns]
    datatrainCategorical = datatrain.loc[:, categoricalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestCategorical = datatest[:, categoricalColumnsIdx]
    else:
        datatestCategorical = datatest.loc[:, categoricalColumns.columns].values

    # Compare each test sample with all train instances
    allDistances = np.zeros((test.shape[0], train.shape[0]))
    for i, (testSampleNumerical, testSampleCategorical) in enumerate(zip(datatestNumerical, datatestCategorical)):

        # Numerical data
        numeratorNumerical = (datatrainNumerical * testSampleNumerical)
        numeratorNumerical = np.sum(weights[numericalColumnsIdx] * numeratorNumerical, axis=1)
        denominatorNumericalTrain = np.sum(weights[numericalColumnsIdx] * datatrainNumerical**2, axis=1)
        denominatorNumericalTest  = np.sum(weights[numericalColumnsIdx] * testSampleNumerical**2)

        # Categorical data
        numeratorCategorical = np.uint8(datatrainCategorical == testSampleCategorical)
        numeratorCategorical = np.sum(weights[categoricalColumnsIdx] * numeratorCategorical, axis=1)
        denominatorCategoricalTrain = np.sum(weights[categoricalColumnsIdx] * np.ones(datatrainCategorical.shape), axis=1)
        denominatorCategoricalTest  = np.sum(weights[categoricalColumnsIdx] * np.ones(testSampleCategorical.shape))

        # Cosine distances
        numerator = numeratorNumerical + numeratorCategorical
        denominatorTrain = np.sqrt(denominatorNumericalTrain + denominatorCategoricalTrain)
        denominatorTest  = np.sqrt(denominatorNumericalTest + denominatorCategoricalTest)
        denominator = denominatorTrain * denominatorTest

        # Distances
        numerator[denominator == 0] = 0         # avoid invalid values
        denominator[denominator == 0] = 1
        cosineSim = numerator/denominator
        allDistances[i, :] = 1 - cosineSim

    return allDistances


def canberra(train, test, weights):
    """
        Args:
            - train: Training data instances.
            - test: Testing data instances.
            - weights: Weights of each feature of the instances
    """

    # Train and test samples
    datatrain = train.copy()
    datatest = test.copy()

    # Numerical columns and values
    numericalColumns = datatrain.select_dtypes('number')
    numericalColumnsIdx = [datatrain.columns.get_loc(c) for c in numericalColumns.columns]
    datatrainNumerical = datatrain.loc[:, numericalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestNumerical = datatest[:, numericalColumnsIdx]
    else:
        datatestNumerical = datatest.loc[:, numericalColumns.columns].values

    # Categorical columns and values
    categoricalColumns = datatrain.select_dtypes('object')
    categoricalColumnsIdx = [datatrain.columns.get_loc(c) for c in categoricalColumns.columns]
    datatrainCategorical = datatrain.loc[:, categoricalColumns.columns].values
    if isinstance(datatest, np.ndarray):
        datatestCategorical = datatest[:, categoricalColumnsIdx]
    else:
        datatestCategorical = datatest.loc[:, categoricalColumns.columns].values

    # Compare each test sample with all train instances
    allDistances = np.zeros((test.shape[0], train.shape[0]))
    for i, (testSampleNumerical, testSampleCategorical) in enumerate(zip(datatestNumerical, datatestCategorical)):

        # Numerical data
        numeratorNumerical = np.abs(datatrainNumerical - testSampleNumerical)
        denominatorNumerical = np.abs(datatrainNumerical + testSampleNumerical)

        # Ratio (numerical data)
        numeratorNumerical[denominatorNumerical == 0] = 0           # avoid invalid values
        denominatorNumerical[denominatorNumerical == 0] = 1
        divisionNumerical = numeratorNumerical / denominatorNumerical
        divisionNumerical = np.sum(weights[numericalColumnsIdx] * divisionNumerical, axis=1)

        # Categorical data
        numeratorCategorical = np.uint8(datatrainCategorical != testSampleCategorical)
        denominatorCategorical = np.uint8(datatrainCategorical != testSampleCategorical)

        # Ratio (categorical data)
        numeratorCategorical[denominatorCategorical == 0] = 0       # avoid invalid values
        denominatorCategorical[denominatorCategorical == 0] = 1
        divisionCategorical = numeratorCategorical / denominatorCategorical
        divisionCategorical = np.sum(weights[categoricalColumnsIdx] * divisionCategorical, axis=1)

        # Distances
        allDistances[i, :] = divisionNumerical + divisionCategorical

    return allDistances
