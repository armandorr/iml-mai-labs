"""
File that defines the different weighting available
"""

import numpy as np
import pandas as pd
from sklearn.feature_selection import mutual_info_classif
import sklearn_relief as relief
from preprocessing.preprocessingUtils import encodeCategorical

def equalWeight(trainMatrix):
    '''
        Args:
            - trainMatrix: Training data instances.

        Equal Weight, all the features are considered all of them are equal and then assign a weight value
        of 1.0 to all the attributes
    '''
    return np.ones(shape=trainMatrix.shape[1])


def informationGainWeight(trainMatrix, target, num_neighbors):
    '''
        Args:
            - trainMatrix: Training data instances.
            - target: Training data labels.
            - num_neighbors: Number of neighbors to use for MI estimation for continuous variables.
                             Higher values reduce variance of the estimation, but could introduce a bias.

        Estimate mutual information between each feature and the target.
    '''

    trainMatrix, cols = encodeCategorical(trainMatrix)
    if not cols: cols = 'auto'
    if isinstance(target, pd.Series): target = target.values

    # Compute weights
    try:
        # Only for the cases where Mutual Information cannot perform a reliable analysis or computation
        mi = mutual_info_classif(np.array(trainMatrix.values), np.array(target), discrete_features=cols,
                               n_neighbors=num_neighbors, random_state=1000) # random value fixed
    except:
        # Use equal weights instead
        mi = np.ones(trainMatrix.shape[1])

    return mi


def reliefWeight(trainMatrix, target, n_features_to_select):
    '''
        Args:
            - trainMatrix: Training data instances.
            - target: Training data labels.
            - n_features_to_select: number of best features to retain after the feature selection process.
                                    The "best" features are the highest-scored features according to the ReliefF
                                    scoring process.

        Relief is the most basic of the Relief-based feature selection algorithms, and it requires you to
        specify the number of nearest neighbors to consider in the scoring algorithm.
        (See: https://epistasislab.github.io/scikit-rebate/using/)
        (See: https://gitlab.com/moongoal/sklearn-relief)
        (See: https://libraries.io/pypi/sklearn-relief)
    '''

    if isinstance(target, pd.Series): target = target.values

    trainMatrix, cols = encodeCategorical(trainMatrix)
    rf = relief.Relief(n_features=n_features_to_select)
    rf.fit_transform(np.array(trainMatrix.values), np.array(target))

    return rf.w_

