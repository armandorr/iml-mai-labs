import os
import numpy as np
import pandas as pd
from scipy.io import arff
import matplotlib.pyplot as plt

from preprocessing import preprocessingUtils


def readData(path):
    '''
        Args:
            - path: Path where to read the dataframe.

        Read and preprocess a dataframe from path.
    '''

    data = arff.loadarff(path)
    dataframe = pd.DataFrame(data[0])
    for col, dtype in dataframe.dtypes.items():
        if dtype == object:  # categorical
            dataframe[col] = dataframe[col].str.decode("utf-8")
    features = dataframe.drop(dataframe.columns[-1], axis=1)
    c = dataframe.columns[-1]
    classes = dataframe[c]

    scaleMethod = 'MinMax' #Normalize all attributes in the range [0,1]. Do not encode the categorical columns
    featuresPreprocessed = preprocessingUtils.preprocessDataframe(features, impute=True, encode=False, verbose=False,
                                                                  scale=True, scaleMethod=scaleMethod)

    return featuresPreprocessed, classes

def read_folds(datasetName, folds=10):
    '''
        Args:
            - datasetName: The name of the dataset to read the folds from.
            - folds: Number of folds to read.

        Read the k first folds of a dataset.
    '''

    TrainMatrix = []; TestMatrix  = []
    TrainLabels = []; TestLabels  = []

    # Read files of each fold
    for i in range(folds):

        # Train data
        train_file= "datasets/" + str(datasetName) + "/" + str(datasetName) + ".fold.00000" + str(i) + ".train.arff"
        matrix, labels = readData(train_file)
        TrainMatrix.append(matrix)
        TrainLabels.append(labels)

        # Test data
        test_file = "datasets/" + str(datasetName) + "/" + str(datasetName) + ".fold.00000" + str(i) + ".test.arff"
        matrix, labels = readData(test_file)
        TestMatrix.append(matrix)
        TestLabels.append(labels)

    return TrainMatrix, TrainLabels, TestMatrix, TestLabels

def describeFeatures(features, outpath):
    '''
        Args:
            - features: Features to describe.
            - outpath: Path where to save the results.

        Function that describe the features of a dataset and save the results.
    '''

    for column, dtype in features.dtypes.items():
        if dtype == object:  # categorical
            labels, counts = np.unique(features[column].values, return_counts=True)
            plt.pie(counts, labels=labels)
            plt.title('Variable ' + column + ' categories')
        else:  # numeric
            plt.hist(features[column])
            plt.xlabel(column + ' values')
            plt.ylabel('Quantity')
            plt.title('Variable ' + column + ' distribution')

        plt.savefig(outpath + column)
        plt.show()

def checkAndCreateFolder(path, verbose=False):
    '''
        Args:
            - path: Path that has to be checked and created.
            - verbose (bool): True if you want verbose information about the process, false otherwise

        Function that creates all the folders in order to be able to create a specific path folder.
    '''

    split = path.split("/")
    for i in range(len(split)-1):
        path = "/".join(split[:i+1])
        if not os.path.exists(path):
            os.mkdir(path)
            if verbose:
                print("Folder "+path+" created.")
