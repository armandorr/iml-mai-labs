import math
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler


def preprocessDataframe(dataframe, impute=True, encode=True, scale=True, scaleMethod='MinMax', verbose=False):
    '''
        Args:
            - dataframe: Dataframe to preprocess.
            - impute: Whether to impute or not the missing values.
            - encode: Whether to encode or not the categorical features.
            - scale: Whether to scale or not the data.
            - scaleMethod: The scale method to use when scale is true
            - verbose (bool): True if you want verbose information about the process, false otherwise

        Preprocess a dataframe with the considered booleans.
    '''
    preprocessedDataframe = dataframe.copy()
    if impute:
        preprocessedDataframe = imputeMissings(preprocessedDataframe, verbose=verbose)
    if encode:
        preprocessedDataframe = encodeCategorical(preprocessedDataframe, verbose=verbose)
    if scale:
        preprocessedDataframe = scaleNumerical(preprocessedDataframe, scaleMethod=scaleMethod, verbose=verbose)

    return preprocessedDataframe


def imputeMissings(dataframe, missingValueCategorical="?", missingValueNumerical=np.nan, margin=20, verbose=False):
    '''
        Args:
            - dataframe: Dataframe to impute the missings.
            - missingValueCategorical: The codification of the missing values for categorical data.
            - missingValueNumerical: The codification of the missing values for numerical data.
            - margin: The percentage of samples in order to consider creating another class or adding them to the most common one
            - verbose (bool): True if you want verbose information about the process, false otherwise

        Impute the missing of a dataframe.
    '''
    dataframeCopy = dataframe.copy()
    for col, dtype in dataframeCopy.dtypes.items():

        missingValue = missingValueNumerical
        if dtype == object:
            missingValue = missingValueCategorical

        # If column has missing values:
        #   - If categorical: Replace them by the maximum category or create another one
        #   - If numerical:   Replace them by the mean of the column
        if any(dataframeCopy[col].isin([missingValue])):
            if dtype == object:  # categorical column
                labels, counts = np.unique(dataframeCopy[col], return_counts=True)
                percentages = counts / np.sum(counts) * 100
                percentagesSorted, labelsSorted = zip(*sorted(zip(percentages, labels), reverse=True))
                if percentagesSorted[0] - percentagesSorted[1] > margin:
                    substitutionValue = labelsSorted[0]
                else:
                    substitutionValue = 'No information'
            else:  # numerical column
                substitutionValue = np.mean(dataframeCopy[col])

            if not isinstance(substitutionValue, str) and math.isnan(substitutionValue):
                dataframeCopy = dataframeCopy.drop(col, axis=1)
                if verbose:
                    print("Deletion of column", col)
            else:
                dataframeCopy[col].replace(missingValue, substitutionValue, inplace=True)
                if verbose:
                    print("Imputation performed on column", col + ":")
                    print("\tImputation of \"" + str(missingValue) + "\" for \"" + str(substitutionValue) + "\"")

    return dataframeCopy


def encodeCategorical(dataframe, verbose=False):
    '''
        Args:
            - dataframe: Dataframe to encode the categorical data.
            - verbose (bool): True if you want verbose information about the process, false otherwise

        Encode the categorical columns using one hot encoding or label encoding.
    '''
    dataframeCopy = dataframe.copy().reset_index(drop=True)
    oneHotColumns = []
    labelColumns = []
    encodedColumns = []
    for colIdx, (col, dtype) in enumerate(dataframeCopy.dtypes.items()):
        if dtype == object:  # categorical
            if len(np.unique(dataframeCopy[col])) > 2:
                oneHotColumns.append(col)
            else:
                labelColumns.append(col)
            encodedColumns.append(colIdx)

    numCatAdded = 0
    if len(oneHotColumns) > 0:
        dataframeCopy, numCatAdded = _encodeCategoricalColumns(dataframeCopy, oneHotColumns, onehot=True)
    for col in labelColumns:
        dataframeCopy, numCatAdded_aux = _encodeCategoricalColumns(dataframeCopy, col, onehot=False)
        numCatAdded += numCatAdded_aux

    if verbose:
        print("Performed categorical encoding:")
        print("\tWith one hot encoding:", oneHotColumns)
        print("\tWith label encoding:", labelColumns)

    lengthNewDf = dataframeCopy.shape[1]
    return dataframeCopy, list(range(lengthNewDf-numCatAdded,lengthNewDf))


def _encodeCategoricalColumns(dataframe, columns, onehot=True):
    '''
        Args:
            - dataframe: Dataframe to encode the categorical columns.
            - columns: Columns to encode the categorical data from.
            - onehot (bool): True if you want to perform one hot encoding. False if prefer label encoding.

        Auxiliar function to encode the categorical columns of a dataframe using one hot encoding or label encoding.
    '''
    encoder = OneHotEncoder() if onehot else LabelEncoder()

    if onehot:
        encodedData = pd.DataFrame(encoder.fit_transform(dataframe[columns]).toarray())
    else:
        enc_data_aux = encoder.fit_transform(dataframe[columns].values)
        encodedData = pd.DataFrame(data=enc_data_aux, columns=[columns])

    dataframe.drop(columns=columns, inplace=True)

    if onehot:
        matchIndexCategory = {}
        idx = 0
        for column, categories in zip(columns, encoder.categories_):
            for category in categories:
                matchIndexCategory[idx] = column + "_" + category
                idx += 1
        encodedData = encodedData.rename(columns=matchIndexCategory)

    resultingDataframe = pd.concat([dataframe, encodedData], axis=1)

    return resultingDataframe, encodedData.shape[1]


def scaleNumerical(dataframe, scaleMethod='MinMax', verbose=False):
    '''
        Args:
            - dataframe: Dataframe to scale the data from.
            - scaleMethod: Scaling method to use.
            - verbose (bool): True if you want verbose information about the process, false otherwise

        Scale a specific dataframe with the desired scaling method.
    '''
    dataframeCopy = dataframe.copy()
    numericalColumns = [col for col, dtype in dataframeCopy.dtypes.items() if dtype != object]
    if scaleMethod == 'MinMax':
        scaler = MinMaxScaler(copy=False)
    else:
        scaler = StandardScaler(copy=False)
    dataframeCopy[numericalColumns] = pd.DataFrame(scaler.fit_transform(dataframeCopy[numericalColumns]),
                                                   columns=numericalColumns)
    if verbose:
        print("Performed scaling on the numerical columns")

    return dataframeCopy
