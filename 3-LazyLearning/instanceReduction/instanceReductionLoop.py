import numpy as np
import pandas as pd

from algorithms.KNN import KNNAlgorithm
from instanceReduction import drop, gcnn, ennth
from preprocessing import dataUtils

def run(reductionTechniqueName, TrainM, TrainL, TestM, TestL, properties, datasetName, savePath):
    """
        Args:
           - reductionTechniqueName (str): The reduction technique to be applied.
           - TrainM: Training data instances.
           - TrainL: Training data labels.
           - TestM: Testing data instances.
           - TestL: Testing data labels.
           - properties: Properties needed for the algorithms. For example the KNN properties for DROP.
           - datasetName (str): The name of the dataset (only for saving purposes).
           - savePath (str): Path to save the results.
    """

    # Thresholds or parameters to use
    thresholds = input("\nSelect the threshold parameters to be used (0 < threshold < 1).\n"
                       "Select more than 2 if you want to be able to produce the Nemenyi plot.\n"
                       "Example: 0.6 0.7 0.8 would perform the test over these thresholds: ")
    thresholds = [float(th) for th in thresholds.split(" ")]
    print(f"Computing {reductionTechniqueName}...")

    # Traverse the selected thresholds
    results = []
    for idxTh, th in enumerate(thresholds):

        # Traverse all the folds
        storages, accuracies, times = [], [], []
        for foldNum, (TrainMatrix, TrainLabel, TestMatrix, TestLabel) in enumerate(zip(TrainM, TrainL, TestM, TestL)):

            # Apply the selected reduction technique
            if reductionTechniqueName == "DROP":
                alg = drop.DROP3(properties['k'], TrainMatrix, TrainLabel, threshold=th,
                                 distance=properties['distance'], voting=properties['voting'],
                                 weighting=properties['weighting'], verbose=False)
            elif reductionTechniqueName == "ENNTH":
                alg = ennth.ENNTh(properties['k'], TrainMatrix, TrainLabel, th)
            else: # GCNN
                alg = gcnn.GCNN(TrainMatrix, TrainLabel, th, verbose=False)

            # Resulting dataset (and percentage of storage)
            dataset, labels = alg.get_newDatasetAndLabels()
            storage = alg.get_storage()

            # Apply the KNN to get the accuracy and execution time
            if dataset.empty:   # Case when the dataset is completely empty
                time = 0
                accuracy = 0
            else:
                knn = KNNAlgorithm(dataset, labels,
                                   k=properties['k'],
                                   distance=properties['distance'],
                                   voting=properties['voting'],
                                   weighting=properties['weighting'])
                accuracy = knn.score(TestMatrix,TestLabel)
                time = knn.meanSolvingTime

            storages.append(storage * 100)
            accuracies.append(accuracy * 100)
            times.append(time)

        # Print results
        print(f"Average results for threshold {th}")
        print("  Accuracy:", np.round(np.mean(accuracies), 4))
        print("  Storage:", np.round(np.mean(storages), 4))
        print("  Time:", np.round(np.mean(times), 4))

        results.append([th] + storages + accuracies + times)

    # Save the results
    numberOfFolds = len(TrainM)
    save = pd.DataFrame(results, columns= ["Threshold"] +
                                          [f"Storage{i}" for i in range(numberOfFolds)] +
                                          [f"Accuracy{i}" for i in range(numberOfFolds)] +
                                          [f"Time{i}" for i in range(numberOfFolds)])

    dataUtils.checkAndCreateFolder(savePath + reductionTechniqueName + "/")
    resultsPath = f"{savePath+reductionTechniqueName}/{datasetName}_{reductionTechniqueName}.csv"
    save.to_csv(resultsPath, index=False)
    print(f"{reductionTechniqueName} for {datasetName} dataset created at {resultsPath}")

    return resultsPath, len(thresholds)
