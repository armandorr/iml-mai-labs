import numpy as np
import pandas as pd
from metrics.distanceMetrics import minkowski


class ENNTh:
    """
       Args:
           - K (int): Number of nearest neighbors.
           - dataset: Data points or instances in DataFrame format.
           - labels: Data labels.
           - threshold: Threshold that indicates the maximum probability to delete a sample
    """

    def __init__(self, K, dataset, labels, threshold):

        self.K = K
        self.dataset = dataset
        self.labels = labels.values
        self.uniqueLabels = np.unique(self.labels)
        self.threshold = threshold

        # Compute belonging probabilities
        delta_k_probs, max_probabilities = self.compute_probs()

        # Run the algorithm
        self.newDataset, self.newLabels = self.run_algorithm(delta_k_probs, max_probabilities)

    def get_storage(self):
        # Get the storage after the reduction
        return len(self.newDataset)/len(self.dataset)

    def get_newDatasetAndLabels(self):
        # Get the reduced datasets and labels
        return self.newDataset, self.newLabels

    def run_algorithm(self, delta_k_probs, max_probabilities):

        # Initialize new dataset
        newDataset = self.dataset.copy()
        newLabels = self.labels.copy()

        # Remove samples
        deleteIndexes = []
        for labelNum, label in enumerate(self.labels):

            # Features
            delta_k_prob = delta_k_probs[labelNum]
            max_probability = max_probabilities[labelNum]

            # For each sample check if the label mismatch or the probability is not high enough
            if delta_k_prob != label or max_probability <= self.threshold:
                deleteIndexes.append(labelNum)

        # Delete the corresponding indices
        newDataset = newDataset.drop(deleteIndexes, axis=0)
        newDataset = newDataset.reset_index(drop=True)

        newLabels = np.delete(newLabels, deleteIndexes, axis=0)
        newLabels = pd.Series(newLabels)

        return newDataset, newLabels

    def compute_probs(self):

        # Distances between all the samples and nearest neighbors
        distances = minkowski(2, self.dataset, self.dataset)
        nearestNeighborIndexesAll = np.argsort(distances, axis=1).astype(int)

        delta_k_probs = []
        max_probabilities = []
        for sampleNum, indexesNearestNeighbors in enumerate(nearestNeighborIndexesAll):

            # Sorted K-nearest neighbors (ignore the first one because it's itself)
            indexesNearestNeighbors = indexesNearestNeighbors[1:]
            indexesNearestNeighbors = indexesNearestNeighbors[:self.K]

            # Get the according distances
            distancesNearestNeighbors = distances[sampleNum][indexesNearestNeighbors]

            # Get the according labels
            labelsNearestNeighbors = self.labels[indexesNearestNeighbors]

            # Set of probabilities Pi(x) (sample x belongs to a class i)
            Pixs = []
            for label in self.uniqueLabels:
                Pixs.append(np.sum((labelsNearestNeighbors == label)/(1+distancesNearestNeighbors)))

            # Normalize Pi(x)'s
            pixs = Pixs/np.sum(Pixs)

            # Get the class with higher probability
            dkprob = np.argsort(-pixs)[0]  # reversed argsort
            maxProbability = pixs[dkprob]

            delta_k_probs.append(self.uniqueLabels[dkprob])
            max_probabilities.append(maxProbability)

        return delta_k_probs, max_probabilities
