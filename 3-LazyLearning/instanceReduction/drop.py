import numpy as np

from instanceReduction import ennth
from algorithms import KNN
from metrics.distanceMetrics import minkowski


class DROP3:
    """
        Args:
            - K (int): Number of nearest neighbors.
            - dataset: Data points or instances in DataFrame format.
            - labels: Data labels.
            - threshold: Threshold of the ENNTh algorithm that has to be applied
            - distance (str): Distance function used to be used in KNN.
                   - 'euclidean' (default): Minkowski p=2 distance.
                   - 'cosine': Cosine distance (from cosine similarity).
                   - 'canberra': Canberra distance.
            - voting (str): The voting scheme to be used in KNN.
                   - 'majority': Majority voting scheme.
                   - 'inverseDistance': Inverse distance voting.
                   - 'sheppard': Sheppard voting scheme.
            - weighting (str): The weighting method to be used in KNN
                   - 'equal': All the features will be weighted equally.
                   - 'informationGain': Weight features by the information they provide.
                   - 'relief': Relief weighting procedure.
            - verbose (bool): True if you want verbose information about the process, false otherwise
    """
    def __init__(self, K, dataset, labels, threshold, distance='euclidean', voting='majority', weighting='equal', verbose=False):

        # Variables and parameters
        self.K = K
        self.original_dataset = dataset
        self.original_labels = labels
        self.threshold = threshold
        self.verbose = verbose

        self.distance = distance
        self.voting = voting
        self.weighting = weighting

        # Run the algorithm
        self.run_algorithm()

    def get_storage(self):
        # Get the new storage after the reduction
        return len(self.filter_dataset)/len(self.original_dataset)

    def get_newDatasetAndLabels(self):
        # Get the datasets after the reduction
        return self.filter_dataset, self.filter_labels

    def run_algorithm(self):

        # Apply ENNTH to remove noisy instances (noise-filtering pass)
        enn = ennth.ENNTh(self.K, self.original_dataset, self.original_labels, self.threshold)
        self.new_dataset, self.new_labels = enn.get_newDatasetAndLabels()

        # Apply DROP2 with the modifications
        self.filter_dataset, self.filter_labels = self.drop2(self.new_dataset, self.new_labels)

    def drop2(self, X_train, y_train):

        # Input instances
        Sx = X_train.copy()
        Sy = y_train.copy()

        # Create the list of neighbors and associates
        kNeighbors = [set() for _ in range(len(X_train))]
        associates = [set() for _ in range(len(X_train))]

        # Sort distances between points
        distances = minkowski(2, X_train, X_train)
        nearestNeighborIndexesAll = np.argsort(distances, axis=1).astype(int)

        # Find first enemy of each sample
        distances = []
        for sampleNum, indexesNearestNeighbors in enumerate(nearestNeighborIndexesAll):

            # Sorted nearest neighbors (ignore the first one because it's itself)
            indexesNearestNeighbors = indexesNearestNeighbors[1:]

            # Current sample features and label
            myValues = X_train.iloc[sampleNum]
            myLabel = y_train.iloc[sampleNum]

            # Find the first enemy and store its distance
            nearestEnemyDistance = 0
            for index in indexesNearestNeighbors:
                if myLabel != y_train.iloc[index]:
                    nearestEnemyDistance = minkowski(2, myValues, X_train.iloc[index])
                    nearestEnemyDistance = nearestEnemyDistance[0][0]
                    break
            distances.append(nearestEnemyDistance)

        # Samples sorted by the further enemy
        indexesToSort = np.argsort(-np.array(distances))

        X_train = X_train.iloc[indexesToSort]
        X_train_reset = X_train.copy()
        X_train_reset.reset_index(drop=True,inplace=True)

        y_train = y_train.iloc[indexesToSort]
        y_train_reset = y_train.copy()
        y_train_reset.reset_index(drop=True, inplace=True)

        # Compute associates and k-Neighbors
        distances = minkowski(2, X_train_reset, X_train_reset)
        nearestNeighborIndexesAll = np.argsort(distances, axis=1).astype(int)

        for sampleNum, indexesNearestNeighbors in enumerate(nearestNeighborIndexesAll):

            # Sorted nearest neighbors (ignore the first one because it's itself)
            indexesNearestNeighbors = indexesNearestNeighbors[1:]

            # Get the first K+1 neighbors
            indexesNearestNeighbors = indexesNearestNeighbors[:self.K+2]
            kNeighbors[sampleNum] = set(indexesNearestNeighbors)

            # Add P to each of its neighbors’ lists of associates.
            for index in indexesNearestNeighbors:
                associates[index].add(sampleNum)

        # Run throughout the dataset samples
        delSet = set()
        deletions = 0
        for idxIt, rowIdx in enumerate(X_train_reset.index):

            # Get the associates data
            associatesIndexes = list(associates[idxIt])
            associatesValues = X_train_reset.iloc[associatesIndexes]
            labelsTrue = y_train_reset.iloc[associatesIndexes].values

            # With count
            bestKnn_With = KNN.KNNAlgorithm(X_train_reset, y_train.values, self.K+1, distance=self.distance,
                                            voting=self.voting, weighting=self.weighting)
            labelsPredWith = bestKnn_With.classify(associatesValues)
            numWith = np.sum(labelsPredWith == labelsTrue)

            # Without count
            X_train_without = X_train_reset.copy()
            y_train_without = y_train_reset.copy()
            X_train_without.drop(idxIt, inplace=True)
            y_train_without.drop(idxIt, inplace=True)

            bestKnn_Without = KNN.KNNAlgorithm(X_train_without, y_train_without.values, self.K+1, distance=self.distance,
                                               voting=self.voting, weighting=self.weighting)
            labelsPredWithout = bestKnn_Without.classify(associatesValues)
            numWithout = np.sum(labelsPredWithout == labelsTrue)

            # Sample is not needed and will be discarded
            if numWithout >= numWith:

                deletions += 1
                if self.verbose: print(f"Iteration {idxIt} ({deletions} out of {len(X_train.values)} deletions)")
                delSet.add(rowIdx)

                # Remove from original dataset
                Sx.drop(rowIdx, inplace=True)
                Sy.drop(rowIdx, inplace=True)

                # Add new neighbors to the associates list affected by the deletion of the sample
                associates_aux = associates.copy()
                for associate in associates[idxIt]:

                    neighbors = [v for v in nearestNeighborIndexesAll[associate][1:] if v not in delSet]
                    newNeighbors = neighbors[:self.K+1]

                    kNeighbors[associate] = newNeighbors
                    for newNeighbor in newNeighbors:
                        associates_aux[newNeighbor].add(associate)

                associates = associates_aux
            else:
                if self.verbose: print("Not deleted")

        return Sx.reset_index(drop=True), Sy.reset_index(drop=True)
