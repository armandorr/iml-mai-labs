import numpy as np
import pandas as pd
from metrics.distanceMetrics import minkowski

import itertools
from math import factorial


class GCNN:
    """
       Args:
           - dataset: Data points or instances in DataFrame format.
           - labels: Data labels.
           - p: difficulty of the absorption criterion to be satisfied
           - verbose (bool): True if you want verbose information about the process, false otherwise
    """

    def __init__(self, dataset, labels, p, verbose=False):

        # Save original dataset and label (fixed values)
        self.originalDataset = dataset
        self.originalLabels = np.array(labels)
        self.uniqueLabels = np.unique(labels)

        # Input variables and parameters
        self.dataset = dataset
        self.labels = np.array(labels)
        self.p = p
        self.verbose = verbose

        # Prototypes (new dataset)
        self.prototypes = []
        self.prototypesLabels = []

        # Indices
        self.datasetInd = np.arange(self.dataset.shape[0])
        self.prototypesInd = []

        # Distances all instances
        self.distancesAll = minkowski(2, self.originalDataset, self.originalDataset)

        # Initial prototypes
        self.initial_prototypes()

        # Run the algorithm
        self.newDataset, self.newLabels = self.run_algorithm()

    def get_storage(self):
        # Get the new storage after the reduction
        return len(self.newDataset)/len(self.originalDataset)

    def get_newDatasetAndLabels(self):
        # Get the datasets after the reduction
        return self.newDataset, self.newLabels

    def select_prototype(self, samplesInd):

        # Distances between samples of the same class
        distances = self.distancesAll[samplesInd][:,samplesInd]

        # Nearest neighbor of each sample (k=1)
        nearestNeighborIndexes = np.argsort(distances, axis=1).astype(int)
        if nearestNeighborIndexes.shape[1] > 1:
            nearestNeighborIndexes = nearestNeighborIndexes[:, 1]
        else:
            nearestNeighborIndexes = nearestNeighborIndexes[:, 0]

        # Most "voted" nearest neighbor
        uniqueIndexes, counts = np.unique(nearestNeighborIndexes, return_counts=True)
        mostVotedIndex = uniqueIndexes[np.argmax(counts)]

        return mostVotedIndex

    def initial_prototypes(self):

        # Initial prototype of each label
        prototypesAdded = []
        for label in self.uniqueLabels:

            # Samples same label
            samplesLabelIndices = np.where(self.labels == label)[0]
            samplesLabelInd = self.datasetInd[samplesLabelIndices]

            # Prototype
            prototypeIndex = self.select_prototype(samplesLabelInd)
            prototypeLabel = self.dataset.iloc[samplesLabelIndices[prototypeIndex]]
            self.prototypes.append(prototypeLabel)
            self.prototypesLabels.append(label)

            # Keep it as a new prototype
            datasetIndex = samplesLabelIndices[prototypeIndex]
            prototypesAdded.append(datasetIndex)

        # Remove the prototypes from the original dataset
        self.dataset = self.dataset.drop(prototypesAdded, axis=0)
        self.dataset = self.dataset.reset_index(drop=True)
        self.labels = np.delete(self.labels, prototypesAdded, axis=0)
        self.datasetInd = np.delete(self.datasetInd, prototypesAdded)

        # Prototypes to the new dataset
        self.prototypes = np.array(self.prototypes)
        self.prototypesLabels = np.array(self.prototypesLabels)
        self.prototypesInd = np.array(prototypesAdded)


    def run_algorithm(self):

        # Absorption check (unabsorbed samples)
        unabsorbedSamplesIndices, allAbsorbed = self.all_absorbed()

        # Iterative process until all samples are absorbed
        total_executions = 0
        while not allAbsorbed:

            # Print intermediate results
            if self.verbose:
                if (total_executions % 25) == 0:
                    print('Execution:', total_executions+1)
                    print('   Prototypes:', len(self.prototypesLabels))
                    print('   Remaining samples:', len(self.labels))

            # For each label, add a new prototype from the corresponding unabsorbed samples
            prototypesAdded = []
            for i, (label, unabsorbedSamplesIndicesLabel) in enumerate(zip(self.uniqueLabels, unabsorbedSamplesIndices)):

                # If not unabsorbed samples, continue with the next label
                if len(unabsorbedSamplesIndicesLabel) == 0:
                    continue

                # Unabsorbed samples of this label
                unabsorbedSamplesInd = self.datasetInd[unabsorbedSamplesIndicesLabel]

                # Prototype of the current label
                prototypeLabelIndex = self.select_prototype(unabsorbedSamplesInd)
                prototypeLabel = self.dataset.iloc[unabsorbedSamplesIndicesLabel[prototypeLabelIndex]].values

                # Add the prototype to the new dataset
                self.prototypes = np.append(self.prototypes, [prototypeLabel], axis=0)
                self.prototypesLabels = np.append(self.prototypesLabels, label)
                self.prototypesInd = np.append(self.prototypesInd, unabsorbedSamplesIndicesLabel[prototypeLabelIndex])

                # Prototypes to the new dataset
                datasetIndex = unabsorbedSamplesIndicesLabel[prototypeLabelIndex]
                prototypesAdded.append(datasetIndex)

            # Remove the new prototypes from the original dataset
            self.dataset = self.dataset.drop(prototypesAdded, axis=0)
            self.dataset = self.dataset.reset_index(drop=True)
            self.labels = np.delete(self.labels, prototypesAdded, axis=0)
            self.datasetInd = np.delete(self.datasetInd, prototypesAdded)

            # Absorption check (unabsorbed samples)
            unabsorbedSamplesIndices, allAbsorbed = self.all_absorbed()

            total_executions += 1

        # Convert to dataframe
        newDataset = pd.DataFrame(self.prototypes, columns=self.originalDataset.columns)
        newDataset = newDataset.astype(self.originalDataset.dtypes)
        newLabels = pd.Series(self.prototypesLabels)

        return newDataset, newLabels


    def minimal_distance_heterogeneous_samples(self):

        # Number of instances
        N = len(self.labels)

        # Find the min. distance between samples with different label
        if len(np.unique(self.labels)) > 1:

            # Heterogeneous samples
            n = int(factorial(N) / (factorial(2) * factorial(N-2)))
            heter = [l1 != l2 for l1, l2 in itertools.combinations(self.labels, 2)]

            # Indices heterogeneous samples in distance matrix
            I, J = np.triu_indices(len(self.datasetInd), k=1)
            I, J = I[heter], J[heter]

            # Minimum distance between heterogeneous samples
            distancesSamples = self.distancesAll[self.datasetInd[I], self.datasetInd[J]]
            minDistance = np.min(distancesSamples)

        else:
            minDistance = 0

        return minDistance


    def all_absorbed(self):

        # Distances samples with prototypes
        distances = self.distancesAll[self.datasetInd][:, self.prototypesInd]

        # Minimal distance between heterogeneous samples
        delta = self.minimal_distance_heterogeneous_samples()

        # Samples strongly absorbed
        unabsorbedSamplesIndices = []
        allAbsorbed = True
        for i, label in enumerate(self.uniqueLabels):

            # Samples same label
            samplesIndices = np.where(self.labels == label)[0]
            samples = self.dataset.iloc[samplesIndices]

            # Homogeneous prototypes
            homogeneousIndices = np.where(self.prototypesLabels == label)[0]

            # Distances to nearest homogeneous prototypes
            homogeneousDistances = distances[samplesIndices[:, np.newaxis], homogeneousIndices]
            homogeneousNNIndices = np.argmin(homogeneousDistances, axis=1)
            homogeneousNNDiff = distances[samplesIndices, homogeneousNNIndices]

            # Heterogeneous prototypes
            heterogeneousIndices = np.where(self.prototypesLabels != label)[0]

            # Distances to nearest heterogeneous prototypes
            if len(heterogeneousIndices) > 0:

                heterogeneousDistances = distances[samplesIndices[:, np.newaxis], heterogeneousIndices]
                heterogeneousNNIndices = np.argmin(heterogeneousDistances, axis=1)
                heterogeneousNNDiff = distances[samplesIndices, heterogeneousNNIndices]

            else:
                heterogeneousNNDiff = np.zeros(len(samplesIndices))

            # Strongly absorbed condition
            absorbed = (heterogeneousNNDiff - homogeneousNNDiff) > (self.p * delta)

            # Unabsorbed samples of the current label
            if not absorbed.all():
                allAbsorbed = False
                unabsorbedSamplesIndices.append(samplesIndices[absorbed == False])
            else:
                unabsorbedSamplesIndices.append([])

        return unabsorbedSamplesIndices, allAbsorbed
