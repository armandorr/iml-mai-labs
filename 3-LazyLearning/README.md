# Exercise number 3
### Lazy learning
Introduction to Machine Learning (IML) - Master in Artificial Intelligence

## Team number 4

Composed by:
- Blanca Llauradó Crespo
- Sonia Rabanaque Rodríguez
- Armando Rodriguez Ramos
- Anna Sallés Rius

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.7.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies. Orange3 package may give some problems. However, only compute the Nemenyi's plots. Discard these steps in the menu and comment the import Orange in the ``algorithms/nemenyi.py`` file if the Orange3 package is not correctly installed.
```bash
pip install -r requirements.txt
```
you can check if dependencies were installed by running next
command,it should print list with installed dependencies
```bash
pip list
```
5. Close virtual env
```bash
deactivate
```
## Execute scripts
1. open virtual env
```bash
source venv/bin/activate
```
2. Running the script

Any configuration of the K-Nearest Neighbor (K-NN) and instance reduction approach can be selected as well as its parameters.
When executing `main.py` file as mentioned below, the terminal displays several possible options depending on the options chosen.

- First of all, the terminal will show the possible datasets to use. Although it is possible to choose any of the datasets provided, we strongly recommend to use the ones already analyzed in the report: {zoo, satimage, pen-based}. We highly recommend using `zoo` as it's not highly time-consuming.
- Then, it is possible to choose the number of folds to use, a value from 1 to 10.
- After that, it is possible to decide if the user wants to apply the K-Nearest Neighbors algorithm using the previous selected folds or not. If you agree with that, the display will ask you for the configuration to use to apply this technique.
    - First, you can introduce the number of nearest neighbors (K) to take into account to assign the result. The available options are 1, 3, 5 and 7.
    - Then, the distance metric has to be introduced from the following options: Euclidean, Cosine or Canberra.
    - The next parameter to be specified is the voting scheme to decide the resulting class. The possibilities are: Majority, Inverse Distance and Sheppard.
    - Finally, the weighting method has to be selected between equal, Information Gain or Relief. 
- Another option is to find the best K-NN configuration, by trying all the possible combinations using the previous values for each variable.
  - Once this process has finished, the display will ask if you want to create a Nemenyi's plot with the previous results.
- Finally, you can apply an instance reduction technique to remove some samples from the original dataset. If you want to do it, it is necessary to select the configuration to be used. In case a previous K-NN has been done previously, you can use the same parameters selected before. Otherwise, it is necessary to specify the configuration to be applied for the reduction techniques. Moreover, the display will ask you to select one of the instance reduction approaches that we have implemented: GCNN, ENNTh or DROP3.
  - After that, it is necessary to select the threshold parameters to be used. In case you want to visualize a Nemenyi's plot, you have to introduce more than 2 threshold values.
  - Then, you can decide if you want to create a Nemenyi's plot for the reduction results and, if you want to do it, the variables to compute it from accuracy, storge and time.


 ```bash
 python3 main.py
 ```
3. Close virtual env
```bash
deactivate
```