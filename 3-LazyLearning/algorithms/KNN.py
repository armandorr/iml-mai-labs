import time
import numpy as np

from metrics.distanceMetrics import minkowski, cosine, canberra
from metrics.weightingFeatures import equalWeight, informationGainWeight, reliefWeight
from metrics.votingSchemes import majorityClass, distanceWeightedMethods


class KNNAlgorithm:
    """
        Args:
          - TrainMatrix: Folds of training data instances.
          - TrainLabels: Folds of training data labels.
          - k (int): Number of neighbors to use
          - distance (str): Distance to be used. Options are euclidean, cosine and canberra
          - voting (str): Voting schema to be used. Options are majority, inverseDistance and sheppard
          - weighting (str): Weighting mechanism to be used. Options are equal, informationGain and relief

        The class constructor initializes the KNN algorithm.
    """

    def __init__(self, TrainMatrix, TrainLabels, k, distance='euclidean',
                 voting='majority', weighting='equal'):

        # Training data, number of neighbors and solving time
        self.dataset = TrainMatrix
        self.labels = TrainLabels
        self.k = k
        self.meanSolvingTime = 0
        self.weighting = weighting

        # Distance/similarity metric
        assert distance in ['euclidean', 'cosine', 'canberra'], "The distance name \""+distance+"\" is not correct"
        if distance == 'euclidean':
            self.distance_function = lambda s, c, w: minkowski(2, s, c, w)   # Minkowski (r=2, Euclidean)
        elif distance == 'cosine':
            self.distance_function = lambda s, c, w: cosine(s, c, w)
        elif distance == 'canberra':
            self.distance_function = lambda s, c, w: canberra(s, c, w)

        # Voting scheme
        assert voting in ['majority', 'inverseDistance', 'sheppard']
        if voting == 'majority':
            self.voting_scheme = lambda c, d, l: majorityClass(c, d, l)
        elif voting == 'inverseDistance':
            self.voting_scheme = lambda c, d, l: distanceWeightedMethods(c, d, l, methodType="inverseDistance")
        elif voting == 'sheppard':
            self.voting_scheme = lambda c, d, l: distanceWeightedMethods(c, d, l, methodType="sheppardWork")

        # Weighting approach
        assert weighting in ['equal', 'informationGain', 'relief']
        if weighting == 'equal':
            self.weighting_features = lambda d: equalWeight(d)
        elif weighting == 'informationGain':
            self.weighting_features = lambda d, l, n: informationGainWeight(d, l, n) #(trainMatrix, target, num_neighbors)
        elif weighting == 'relief':
            self.weighting_features = lambda d, l, n: reliefWeight(d, l, n) #(trainMatrix, target, n_features_to_select)

    def classify(self, TestMatrix):
        # Function that classifies the TestMatrix, i.e. function that predicts or inference the labels of the TestMatrix

        start_time = time.time()

        # Compute weights
        if self.weighting == 'equal':
            featuresWeights = self.weighting_features(self.dataset)
        elif self.weighting == 'informationGain':
            featuresWeights = self.weighting_features(self.dataset, self.labels, self.k)
        else:
            featuresWeights = self.weighting_features(self.dataset, self.labels, self.dataset.shape[1])

        # Distance/similarity train and test samples
        # M x N, where M is the number of test vectors and N is the number of instances in the train
        distancesSamples = self.distance_function(self.dataset, TestMatrix, featuresWeights)

        # Determine class of each test sample
        labelsTest = self.voting_scheme(self.k, distancesSamples, self.labels)

        # Mean solving time for observation
        finish_time = time.time()
        self.meanSolvingTime = (finish_time - start_time)

        return labelsTest

    def score(self, TestMatrix, TestLabel):
        # Function that classifies and obtain the score of TestMatrix using the real TestLabel
        labelsPred = self.classify(TestMatrix)
        return (np.sum(TestLabel.values == labelsPred)) / len(TestLabel.values)
