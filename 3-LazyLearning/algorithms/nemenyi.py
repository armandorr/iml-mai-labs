import Orange
import itertools
import numpy as np
import pandas as pd
from scipy.stats import friedmanchisquare, rankdata, studentized_range

from preprocessing import dataUtils


def nemenyiKNN(accuraciesPath, timesPath, datasetName, verbose=False):
    """
        Args:
          - accuraciesPath: Path of the accuracy results dataset for best KNN computation
          - timesPath: Path of the time results dataset for best KNN computation
          - datasetName (str): The name of the dataset (only for saving purposes).
          - verbose (bool): True if you want verbose information about the process, false otherwise

        This function computes and saves the Nemenyi plot of the datasets given by the paths (KNN).
    """

    # Get the datasets (previous results)
    path = "/".join(accuraciesPath.split("/")[:-2])+"/bestKNN/"
    df_acc  = pd.read_csv(accuraciesPath).values
    df_time = pd.read_csv(timesPath).values
    df = np.concatenate((-df_acc,df_time),axis=1)

    # Compute friedman p-value
    _, pvalue = friedmanchisquare(*df)
    if verbose: print("Friedman p-value:", pvalue)

    # Rank the data
    rankedData = rankdata(df, axis=0)
    averageRankedData = np.mean(rankedData,axis=1)

    # Set the names of the combinations
    distance = ['EU-', 'COS-', 'CAN-']
    voting = ['MAJ-', 'INV-', 'SHE-']
    K = ["1", "3", "5", "7"]
    weighting = ['-EQ', '-IG', '-RE']

    # Do the combinations
    combine = [distance, voting, K, weighting]
    combinations = list(itertools.product(*combine))
    names = ["".join(comb) for comb in combinations]

    # Compute the critical distance of the specified statistic
    criticalDistance = _computeCD(0.1, len(combinations), len(df_acc[0]), verbose=verbose)

    # Create and save the plot
    dataUtils.checkAndCreateFolder(path)
    Orange.evaluation.scoring.graph_ranks(averageRankedData, names=names,
                                          cd=criticalDistance, width=10,
                                          textspace=1.5, filename=path+f"{datasetName}_nemenyi.png")
    print(f"Plot saved at path {path+datasetName}_nemenyi.png")


def nemenyiReduction(resultsPath, datasetName, variables, verbose=False):
    """
        Args:
          - resultsPath: Path of the results dataset for an instance reduction method
          - variables: Indicates the desired variables to take into account. The order is accuracy, storage and time.
                       e.g. [True,False,False] would perform the test only over Accuracy
                            while [True,True,False] will perform the test over Accuracy and Storage
          - datasetName (str): The name of the dataset (only for saving purposes).
          - verbose (bool): True if you want verbose information about the process, false otherwise

        This function computes and saves the Nemenyi plot of the datasets given by the paths (instance reduction).
    """

    # Get the paths and data
    split = resultsPath.split("/")[:-1]
    algorithm = split[-1]
    path = "/".join(split)+"/"
    (accuracy, storage, time) = variables

    # Get the datasets (previous results)
    dataset = pd.read_csv(resultsPath)
    N = (len(dataset.columns)-1)//3

    # Select the variables depending on variables parameter
    nameVariables = ("Acc" if accuracy else "") + ("Sto" if storage else "") + ("Tim" if time else "")

    columnNamesAcc = [col for col in list(dataset.columns.values) if col.startswith("Accuracy") and accuracy]
    columnNamesSto = [col for col in list(dataset.columns.values) if col.startswith("Storage") and storage]
    columnNamesTim = [col for col in list(dataset.columns.values) if col.startswith("Time") and time]

    # Obtain the dataset with the desired columns
    dataset = dataset[["Threshold"] + columnNamesAcc + columnNamesSto + columnNamesTim]
    names = ["Threshold" + str(v) for v in dataset["Threshold"].values]

    dataset[columnNamesAcc] = -dataset[columnNamesAcc]  # Bigger better
    dfValues = dataset.values[:, 1:]  # Delete threshold

    # Compute friedman p-value
    _, pvalue = friedmanchisquare(*dfValues)
    if verbose: print("Friedman p-value:", pvalue)

    # Rank the data
    rankedData = rankdata(dfValues, axis=0)
    averageRankedData = np.mean(rankedData, axis=1)

    # Compute the critical distance of the specified statistic
    criticalDistance = _computeCD(0.1, len(averageRankedData), N, verbose=verbose)

    # Create and save the plot
    dataUtils.checkAndCreateFolder(path)
    Orange.evaluation.scoring.graph_ranks(averageRankedData, names=names,
                                          cd=criticalDistance, width=10,
                                          textspace=1.5, filename=f"{path}{datasetName}_{algorithm}_{nameVariables}.png")
    print(f"Plot saved at {path}/{datasetName}_{algorithm}_{nameVariables}.png")


def _computeCD(alpha, k, N, df=1000000, verbose=False): # df infinite
    """
        Args:
          - alpha: confidence value
          - k: Number of points in the distribution
          - N: Number of folds
          - df: location parameter
          - verbose (bool): True if you want verbose information about the process, false otherwise

        Compute the critical distance using the formula of the papers and the computation of the
        studentized_range statistic
    """

    # Critical Distance
    statistic = studentized_range.ppf(1 - alpha, k, df)
    criticalValue = statistic / (2 ** 0.5)          # Divide by sqrt(2)
    criticalDistance = criticalValue * ((k * (k + 1) / (6 * N)) ** 0.5)

    # Print results
    if verbose:
        print(f"Statistic = {statistic} (alpha:{alpha} k:{k} df:{df} N:{N})")
        print(f"Critical value = {criticalValue} = {statistic}/sqrt(2)")
        print(f"Critical distance = {criticalDistance} = {criticalValue}*(({k}*({k}+1)/(6*{N}))**0.5)")

    return criticalDistance