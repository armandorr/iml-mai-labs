import itertools
import numpy as np
import pandas as pd

from preprocessing import dataUtils
from algorithms.KNN import KNNAlgorithm


class BestKNN:
    """
        Args:
           - TrainMatrix: Folds of training data instances.
           - TrainLabels: Folds of training data labels.
           - TestMatrix: Folds of testing data instances.
           - TestLabels: Folds of testing data labels.
           - datasetName (str): The name of the dataset.

        This class computes all the combinations for:
           - Number of neighbors (K): 1, 3, 5 and 7
           - Distances: euclidean, cosine and canberra
           - Voting schemas: majority, inverseDistance and sheppard
           - Weighting mechanisms: equal, informationGain and relief

        And obtain the results for classification accuracy and execution time.
    """

    def __init__(self, TrainMatrix, TrainLabels, TestMatrix, TestLabels, datasetname):
        self.train = TrainMatrix
        self.trainLabels = TrainLabels
        self.test = TestMatrix
        self.testLabels = TestLabels
        self.datasetname = datasetname

    def run(self, savePath):

        # Distance/similarity metric
        distance = ['euclidean', 'cosine', 'canberra']

        # Voting scheme
        voting = ['majority', 'inverseDistance', 'sheppard']

        # K testing values
        K = [1, 3, 5, 7]

        # Weighting approach
        weighting = ['equal', 'informationGain', 'relief']

        # List with all the possible elements to combine
        combine = [distance, voting, K, weighting]
        combinations = list(itertools.product(*combine))

        # Accuracy and time list
        accuracies = []
        times = []

        # Compute all the combinations saving accuracy and time
        for i in range(len(combinations)):
            acc_aux = []
            time_aux = []
            print(f"Combination {combinations[i]}:\t\t\t", end="")
            for j in range(len(self.train)):

                # K-Nearest eighbor with the current configuration
                knn = KNNAlgorithm(self.train[j], self.trainLabels[j], k=combinations[i][2], distance=combinations[i][0],
                                   voting=combinations[i][1], weighting=combinations[i][3])

                # Save metrics (accuracy and time)
                accuracy = knn.score(self.test[j],self.testLabels[j])
                time = knn.meanSolvingTime
                acc_aux.append(accuracy*100)
                time_aux.append(time)

            print(f"{round(np.mean(acc_aux),2)}%")
            accuracies.append(acc_aux)
            times.append(time_aux)

        # Save the results
        accuraciesPath = None
        timesPath      = None
        if savePath:
            fullPath = savePath+"bestKNN/"
            dataUtils.checkAndCreateFolder(fullPath)
            accuraciesPath = fullPath + self.datasetname + "_bestKNN_acc.csv"
            timesPath = fullPath + self.datasetname + "_bestKNN_time.csv"
            save = pd.DataFrame(accuracies, columns=["Fold"+str(i) for i in range(1,len(self.train)+1)])
            save.to_csv(accuraciesPath, index=False)
            save = pd.DataFrame(times, columns=["Fold" + str(i) for i in range(1, len(self.train)+1)])
            save.to_csv(timesPath, index=False)

            print(f"Accuracies dataset created at {accuraciesPath}")
            print(f"Times dataset created at {timesPath}")

        return accuraciesPath, timesPath