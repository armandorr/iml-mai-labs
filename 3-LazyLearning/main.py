import numpy as np

from preprocessing import dataUtils

from algorithms.KNN import KNNAlgorithm
from algorithms.bestKNN import BestKNN
from algorithms.nemenyi import nemenyiKNN, nemenyiReduction

from instanceReduction import instanceReductionLoop

DISTANCES  = {0: "euclidean", 1:"cosine", 2:"canberra"}
VOTINGS    = {0: "majority", 1:"inverseDistance", 2:"sheppard"}
WEIGHTINGS = {0: "equal", 1:"informationGain", 2:"relief"}
REDUCTIONS = {0: "GCNN", 1:"ENNTH", 2:"DROP"}


def main():

    # Read dataset and preprocess
    datasetName = input("Enter the dataset name. We recommend using zoo as it's not highly time-consuming\n"
                        "Analyzed ones are {zoo, satimage, pen-based}: ")

    # Save results
    savePath = "../results/" + datasetName + "/"

    # K-fold cross validation datasets
    numberFolds = int(input("\nIndicate the number of folds to use (from 1 to 10): "))
    TrainM, TrainL, TestM, TestL = dataUtils.read_folds(datasetName, folds=numberFolds)

    foldsText = (str(numberFolds) + " folds") if numberFolds > 1 else "fold"
    knnBool = int(input(f"\nDo you want to compute the K-NN algorithm with the first {foldsText}? Yes[1] / No[0]: ")) == 1

    # K-Nearest Neighbor
    if knnBool:

        # KNN configuration
        properties = getKnnProperties(initialEndl=True)

        print("           Accuracy\t\t Time")
        accuracies, times = [], []
        for foldNum, (TrainMatrix, TrainLabel, TestMatrix, TestLabel) in enumerate(zip(TrainM, TrainL, TestM, TestL)):

            # KNN to the current fold with the specified configuration
            knn = KNNAlgorithm(TrainMatrix, TrainLabel,
                               k=properties['k'], distance = properties['distance'],
                               voting = properties['voting'], weighting = properties['weighting'])

            # Metrics (accuracy and time)
            accuracy = knn.score(TestMatrix, TestLabel)*100
            accuracies.append(accuracy)
            time = knn.meanSolvingTime
            times.append(time)

            print(f"Fold {foldNum + 1}/{numberFolds}:\t{round(accuracy,2)}%\t\t{round(time,4)}s")

        # Mean metrics (fold cross validation)
        meanAccuracy = round(np.mean(accuracies),2)
        meanTime     = round(np.mean(times),4)
        print(f"Average:\t{meanAccuracy}%\t\t{meanTime}s")

    # Best KNN configuration (for the dataset)
    bestKnnBool = int(input(f"\nDo you want to compute the best K-NN search algorithm with the first {foldsText}? Yes[1] / No[0]: ")) == 1
    if bestKnnBool:

        # Find best KNN configuration
        bk = BestKNN(TrainM, TrainL, TestM, TestL, datasetName)
        accuraciesPath, timesPath = bk.run(savePath)

        # Nemenyi's plot with the results of the previous configurations
        bestKnnNemenyiBool = int(input("\nDo you want to create the Nemenyi's plots for the KNN algorithms? Yes[1] / No[0]: ")) == 1
        if bestKnnNemenyiBool:
            nemenyiKNN(accuraciesPath, timesPath, datasetName)

    # Instance reduction
    reductionBool = int(input(f"\nDo you want to compute any instance reduction technique? Yes[1] / No[0]: ")) == 1
    if reductionBool:

        # KNN configuration
        print("\nPlease select the KNN properties that will be performed afterwards.")
        if knnBool:
            propertiesBool = int(input("Do you want to use the same properties"
                                       " used in the starting KNN? Yes[1] / No[0]: ")) == 1
        if not knnBool or not propertiesBool:
            properties = getKnnProperties(initialEndl=False)

        # Instance reduction approach
        reductionTechnique = int(
            input("\nSelect the instance reduction technique to be used. GCNN[0] / ENNTH[1] / DROP3[2]: "))
        reductionTechniqueName = REDUCTIONS[reductionTechnique]

        resultsPath, numThresholds = instanceReductionLoop.run(reductionTechniqueName, TrainM, TrainL,
                                                               TestM, TestL, properties, datasetName, savePath)

        if numThresholds >= 3:
            # Nemenyi's plot with the results of the instance reduction
            reductionNemenyiBool = int(input("\nDo you want to create the Nemenyi's plots for the reduction results? Yes[1] / No[0]: ")) == 1
            if reductionNemenyiBool:
                variables = input("\nIndicate over which variables do you want to compute the Nemenyi plot. The order is Accuracy, Storage and Time."
                          "\nExample: 1 0 0 would perform the test only over Accuracy; while 1 1 0 will perform the test over Accuracy and Storage: ")
                variables = [bool(int(v)) for v in variables.split(" ")]
                nemenyiReduction(resultsPath, datasetName, variables)


def getKnnProperties(initialEndl = False):

    # Default properties
    properties = {"k": 3, "distance": 'euclidean', "voting": "majority", "weighting": "equal"}

    if initialEndl: print() # For visualization purposes

    # K (number of neighbors
    k = input("Select the number of neighbors (K) to be used. (3 by default): ")
    if k != '':
        properties["k"] = int(k)

    # Distance metrics
    distance = input("\nSelect the distance to be used. Euclidean[0] / Cosine[1] / Canberra[2] (0 by default): ")
    if distance != '':
        properties["distance"] = DISTANCES[int(distance)]

    # Voting scheme
    voting = input(
        "\nSelect the voting method to be used. Majority[0] / InverseDistance[1] / Sheppard[2] (0 by default): ")
    if voting != '':
        properties["voting"] = VOTINGS[int(voting)]

    # Weighting features
    weighting = input(
        "\nSelect the weighting method to be used. Equal[0] / InformationGain[1] / Relief[2] (0 by default): ")
    if weighting != '':
        properties["weighting"] = WEIGHTINGS[int(weighting)]

    return properties


if __name__ == '__main__':
    main()
