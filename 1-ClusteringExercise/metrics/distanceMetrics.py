"""
File that defines the different metrics available
"""
import numpy as np


def minkowski(p, sample, centroid):
    if p == np.inf:
        return np.max(abs(sample - centroid), axis=1)
    return np.sum((abs(sample-centroid))**p, axis=1)**(1/p)


def cosine(sample, centroid):
    axis = 0 if len((sample*centroid).shape) == 1 else 1
    numerator = np.sum(sample*centroid, axis=axis)
    denominator = np.sqrt(np.sum(sample**2, axis=axis))*np.sqrt(np.sum(centroid**2))
    return 1-(numerator/denominator if denominator.any() != 0 else 0)
