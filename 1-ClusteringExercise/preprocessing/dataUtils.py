import numpy as np
import pandas as pd
from scipy.io import arff
import matplotlib.pyplot as plt


def readData(path):
    data = arff.loadarff(path)
    dataframe = pd.DataFrame(data[0])
    for col, dtype in dataframe.dtypes.items():
        if dtype == object:  # categorical
            dataframe[col] = dataframe[col].str.decode("utf-8")
    features = dataframe.drop(dataframe.columns[-1], axis=1)
    c = dataframe.columns[-1]
    classes = dataframe[c]
    return features, classes


def describeFeatures(features, outpath):
    for column, dtype in features.dtypes.items():
        if dtype == object:  # categorical
            labels, counts = np.unique(features[column].values, return_counts=True)
            plt.pie(counts, labels=labels)
            plt.title('Variable ' + column + ' categories')
        else:  # numeric
            plt.hist(features[column])
            plt.xlabel(column + ' values')
            plt.ylabel('Quantity')
            plt.title('Variable ' + column + ' distribution')

        plt.savefig(outpath + column)
        plt.show()
