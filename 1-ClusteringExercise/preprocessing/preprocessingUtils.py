import math
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler


def preprocessDataframe(dataframe, impute=True, encode=True, scale=True, verbose=False):
    preprocessedDataframe = dataframe.copy()
    if impute:
        preprocessedDataframe = imputeMissings(preprocessedDataframe, verbose=verbose)
    if encode:
        preprocessedDataframe = encodeCategorical(preprocessedDataframe, verbose=verbose)
    if scale:
        preprocessedDataframe = scaleNumerical(preprocessedDataframe, verbose=verbose)

    return preprocessedDataframe


def imputeMissings(dataframe, missingValueCategorical="?", missingValueNumerical=np.nan, margin=20, verbose=False):
    dataframeCopy = dataframe.copy()
    for col, dtype in dataframeCopy.dtypes.items():

        missingValue = missingValueNumerical
        if dtype == object:
            missingValue = missingValueCategorical

        # If column has missing values:
        #   - If categorical: Replace them by the maximum category or create another one
        #   - If numerical:   Replace them by the mean of the column
        if any(dataframeCopy[col].isin([missingValue])):
            if dtype == object:  # categorical column
                labels, counts = np.unique(dataframeCopy[col], return_counts=True)
                percentages = counts / np.sum(counts) * 100
                percentagesSorted, labelsSorted = zip(*sorted(zip(percentages, labels), reverse=True))
                if percentagesSorted[0] - percentagesSorted[1] > margin:
                    substitutionValue = labelsSorted[0]
                else:
                    substitutionValue = 'No information'
            else:  # numerical column
                substitutionValue = np.mean(dataframeCopy[col])

            if not isinstance(substitutionValue, str) and math.isnan(substitutionValue):
                dataframeCopy = dataframeCopy.drop(col, axis=1)
                if verbose:
                    print("Deletion of column", col)
            else:
                dataframeCopy[col].replace(missingValue, substitutionValue, inplace=True)
                if verbose:
                    print("Imputation performed on column", col + ":")
                    print("\tImputation of \"" + str(missingValue) + "\" for \"" + str(substitutionValue) + "\"")

    return dataframeCopy


def encodeCategorical(dataframe, verbose=False):
    dataframeCopy = dataframe.copy()
    oneHotColumns = []
    labelColumns = []
    for col, dtype in dataframeCopy.dtypes.items():
        if dtype == object:  # categorical
            if len(np.unique(dataframeCopy[col])) > 2:
                oneHotColumns.append(col)
            else:
                labelColumns.append(col)

    if len(oneHotColumns) > 0:
        dataframeCopy = _encodeCategoricalColumns(dataframeCopy, oneHotColumns, onehot=True)
    for col in labelColumns:
        dataframeCopy = _encodeCategoricalColumns(dataframeCopy, col, onehot=False)

    if verbose:
        print("Performed categorical encoding:")
        print("\tWith one hot encoding:", oneHotColumns)
        print("\tWith label encoding:", labelColumns)

    return dataframeCopy


def _encodeCategoricalColumns(dataframe, columns, onehot=True):
    encoder = OneHotEncoder() if onehot else LabelEncoder()

    if onehot:
        encodedData = pd.DataFrame(encoder.fit_transform(dataframe[columns]).toarray())
    else:
        enc_data_aux = encoder.fit_transform(dataframe[columns].values)
        encodedData = pd.DataFrame(data=enc_data_aux, columns=[columns])

    dataframe.drop(columns=columns, inplace=True)

    if onehot:
        matchIndexCategory = {}
        idx = 0
        for column, categories in zip(columns, encoder.categories_):
            for category in categories:
                matchIndexCategory[idx] = column + "_" + category
                idx += 1
        encodedData = encodedData.rename(columns=matchIndexCategory)

    resultingDataframe = pd.concat([dataframe, encodedData], axis=1)

    return resultingDataframe


def scaleNumerical(dataframe, verbose=False):
    dataframeCopy = dataframe.copy()
    numericalColumns = [col for col, dtype in dataframeCopy.dtypes.items() if dtype != object]
    scaler = MinMaxScaler(copy=False)
    dataframeCopy[numericalColumns] = pd.DataFrame(scaler.fit_transform(dataframeCopy[numericalColumns]),
                                                   columns=numericalColumns)
    if verbose:
        print("Performed scaling on the numerical columns")

    return dataframeCopy
