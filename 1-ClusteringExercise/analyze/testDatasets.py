import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from metrics import validationMetrics
from sklearn.preprocessing import LabelEncoder
from preprocessing import preprocessingUtils, dataUtils
from algorithms import Agglomerative, BisectingKMeans, KHarmonicMeans, KMeansUB, PossibilisticCMeans, MeanShiftUB


# This import is just to be able to compare our results with a well implemented algorithm in Sklearn.
# However, it will not be used while computing the results
from sklearn.cluster import KMeans

"""
   Args:
       - dataset (): Data points or instances.
       - method (str): Method that will be use.
                - Kmeans
                - K-harmonic means (KHM)
                - Bisecting k_Means (BKM)
                - Possibilistic C-Means (PCM)
                - Agglomerative
                - Mean Shift.
       - impute (boolean): Data may contain missing values so:
                - True if the dataset has missing values. They will be pre-processed.
                - False otherwise.
       - encode (boolean): Features may have different types so:
                - True if the dataset has categorical values. They will be pre-processed.
                - False otherwise.
       - scale (boolean): Features may contain different ranges ('Min-Max scaling' is used.)
                - True if the features need to be scaled.
                - False otherwise.
       - options (dict): Dictionary with the parameters needed for a required algorithm. 
"""


def executing_and_testing_dataset(dataset=None, method=None, rangeK=None, impute=False,
                                  encode=False, scale=False, save=False, options=None):

    # Read and preprocess selected dataset
    print('Processing', dataset, 'dataset')
    features, classes = dataUtils.readData("datasets/" + str(dataset) + ".arff")
    featuresPreprocessed = preprocessingUtils.preprocessDataframe(features, impute=impute, encode=encode,
                                                                  scale=scale, verbose=False)
    labelsTrue = LabelEncoder().fit_transform(classes.values)
    numRealClasses = len(np.unique(classes))

    # To check the different algorithms (with validation metrics) using the dataset above
    # Creating lists to store the values given by each algorithm
    randIndexs, fowlkes_mallows, calinskis, davies = [], [], [], []

    # Set N random seeds
    # seeds = np.random.randint(0,100000,10)
    seeds = [7296, 15994, 59377, 6918, 29889, 27115, 17736, 7136, 75369, 20987]

    # Set number of clusters to check
    if rangeK is not None: # Don't need for Mean Shift
        num_clusters = range(rangeK[0], rangeK[1]+1)

    # Required for Sklearn Kmeans
    kmeans_kwargs = {"init": "random", "n_init": 10, "max_iter": 300, "random_state": 42}

    # To store the selected algorithm
    m = None

    # To store the accuracy
    accuracies = []
    distributions = []

    # Mean Shift
    if method == 'MS':
        mean_shift_execution(featuresPreprocessed, dataset, labelsTrue, save)
        return

    # Agglomerative
    if method == 'AGG':
        agglomerative_execution(featuresPreprocessed, dataset, num_clusters, labelsTrue, save)
        return

    # Main loop through all the number of clusters
    for it, num_cluster in enumerate(num_clusters):
        print("Number of clusters:", num_cluster)
        randIndexs_aux, fowlkes_mallows_aux, calinskis_aux, silhouettes_aux, davies_aux = [], [], [], [], []

        if method == 'KM_SK':
            # Kmeans Sklearn implementation and validation
            kmeansSklearn = KMeans(n_clusters=num_cluster, **kmeans_kwargs)
            kmeansSklearn.fit(featuresPreprocessed)
            labelsPred = kmeansSklearn.labels_
            randIndexs.append(validationMetrics.rand_index(labelsTrue, labelsPred))
            fowlkes_mallows.append(validationMetrics.fowlkes_mallows(labelsTrue, labelsPred))
            calinskis.append(validationMetrics.calinski_harabasz(featuresPreprocessed, labelsPred))
            davies.append(validationMetrics.davies_bouldin(featuresPreprocessed, labelsPred))

            if num_cluster == numRealClasses:
                accuracies.append(validationMetrics.accuracy_labels(labelsTrue, labelsPred))
                distributions.append(validationMetrics.confussion_matrix_labels(labelsTrue, labelsPred))
        else:
            for seed in seeds:
                print("Seed:", seed)
                if method == 'KM':
                    # KMeans execution
                    distance, iterations = options["distance"], options["iter"]
                    pvalue, initialization = options["p"], options["initialization"]
                    if num_cluster != numRealClasses:
                        m = KMeansUB.KMeansUB(K=num_cluster, dataset=featuresPreprocessed, seed=seed,
                                              distance=distance, p=pvalue, max_iterations=iterations,
                                              kmeans_plus_plus=initialization, verbose=False)
                    else:
                        m = KMeansUB.KMeansUB(K=num_cluster, dataset=featuresPreprocessed, seed=seed,
                                              distance=distance, p=pvalue, max_iterations=iterations,
                                              kmeans_plus_plus=initialization, verbose=False, validationClasses=classes)
                        distribution = m.get_distributions()
                        distributions.append(distribution)
                        accuracies.append(round(validationMetrics.accuracy(distribution), 2))
                elif method == 'KHM':
                    # K-Harmonic Means execution
                    p = options["p"]
                    if num_cluster != numRealClasses:
                        m = KHarmonicMeans.KHarmonicMeans(K=num_cluster, p=p, dataset=featuresPreprocessed,
                                                          seed=seed, verbose=False)
                    else:
                        m = KHarmonicMeans.KHarmonicMeans(K=num_cluster, p=p, dataset=featuresPreprocessed,
                                                          seed=seed, verbose=False, validationClasses=classes)
                        distribution = m.get_distributions()
                        distributions.append(distribution)
                        accuracies.append(round(validationMetrics.accuracy(distribution), 2))
                elif method == 'BKM':
                    # Bisecting K-Means execution
                    iter, clus_to_split = options["iterations"], options["cluster_to_split"]
                    dist, pvalue = options["distance"], options["p"]
                    if num_cluster != numRealClasses:
                        m = BisectingKMeans.BisectingKMeans(K=num_cluster, dataset=featuresPreprocessed, seed=seed,
                                                            iterations=iter, cluster_to_split=clus_to_split,
                                                            distance=dist, p=pvalue, verbose=False)
                    else:
                        m = BisectingKMeans.BisectingKMeans(K=num_cluster, dataset=featuresPreprocessed, seed=seed,
                                                            iterations=iter, cluster_to_split=clus_to_split,
                                                            distance=dist, p=pvalue, verbose=False,
                                                            validationClasses=classes)
                        distribution = m.get_distributions()
                        distributions.append(distribution)
                        accuracies.append(round(validationMetrics.accuracy(distribution), 2))
                elif method == 'PCM':
                    # Possibilistic C-Means execution
                    mvalue, p, max_iter = options["m"], options["p"], options["max_iterations"]
                    if num_cluster != numRealClasses:
                        m = PossibilisticCMeans.PossibilisticCMeans(C=num_cluster, m=mvalue, p=p, seed=seed,
                                                                    dataset=featuresPreprocessed, verbose=False)
                    else:
                        m = PossibilisticCMeans.PossibilisticCMeans(C=num_cluster, m=mvalue, p=p, seed=seed,
                                                                    dataset=featuresPreprocessed, verbose=False,
                                                                    validationClasses=classes)
                        distribution = m.get_distributions()
                        distributions.append(distribution)
                        accuracies.append(round(validationMetrics.accuracy(distribution), 2))

                labelsPred = m.get_labels()

                assert len(np.unique(labelsPred)) != 1, "The selected parameters collapsed all the samples in the same cluster"
                
                randIndexs_aux.append(validationMetrics.rand_index(labelsTrue, labelsPred))
                fowlkes_mallows_aux.append(validationMetrics.fowlkes_mallows(labelsTrue, labelsPred))
                calinskis_aux.append(validationMetrics.calinski_harabasz(featuresPreprocessed, labelsPred))
                davies_aux.append(validationMetrics.davies_bouldin(featuresPreprocessed, labelsPred))

            randIndexs.append(np.mean(randIndexs_aux))
            fowlkes_mallows.append(np.mean(fowlkes_mallows_aux))
            calinskis.append(np.mean(calinskis_aux))
            davies.append(np.mean(davies_aux))

    randIndexs_norm = randIndexs/np.max(randIndexs)
    fowlkes_mallows_norm = fowlkes_mallows/np.max(fowlkes_mallows)
    calinskis_norm = calinskis/np.max(calinskis)
    davies_norm = davies/np.max(davies)

    # Plot all the metrics
    idxRandIndexs = np.argmax(randIndexs)
    if np.max(randIndexs) < 0:
        print("Maximum rand index is below 0")

    plt.figure(1)
    plt.plot(num_clusters, randIndexs_norm, '.-', color='red', label="Adjusted rand index (max)")
    plt.scatter(num_clusters[idxRandIndexs], randIndexs_norm[idxRandIndexs], c='red', marker='*')

    idxFowlkes = np.argmax(fowlkes_mallows)
    plt.plot(num_clusters, fowlkes_mallows_norm, '.-', color='blue', label="Fowlkes Mallows (max)")
    plt.scatter(num_clusters[idxFowlkes], fowlkes_mallows_norm[idxFowlkes], c='blue', marker='*')

    idxCalinski = np.argmax(calinskis)
    plt.plot(num_clusters, calinskis_norm, '.-', color='green', label="Calinski-Harabasz score (max)")
    plt.scatter(num_clusters[idxCalinski], calinskis_norm[idxCalinski], c='green', marker='*')

    idxDavies = np.argmin(davies)
    plt.plot(num_clusters, davies_norm, '.-', color='orange', label="Davies Bouldin (min)")
    plt.scatter(num_clusters[idxDavies],davies_norm[idxDavies],c='orange',marker='*')

    plt.legend()
    plt.title("Comparison number of clusters of "+str(dataset)+" dataset with method "+str(method)+
              ".\nAverage accuracy (among all seeds): "+str(round(np.mean(accuracies), 2))+"%")

    if save:
        check_and_create_folder("../results/")
        check_and_create_folder("../results/" + str(dataset) + "/")
        path = "../results/" + str(dataset)+"/"+str(method)+"-"+str(rangeK[0])+"_"+str(rangeK[1])
        plt.savefig(path,dpi=200)
        print("Plot saved correctly at " + path[3:] + ".png")
    plt.show()

    # Plot heatmap
    idx = np.argmax(accuracies)
    validationMetrics.plot_heatmap(distributions[idx], size=(15, 15), save=save,
                                   path = "../results/"+str(dataset)+"/"+str(method)+"-Matrix_"+str(numRealClasses),
                                   title="Best confussion matrix for " + str(dataset) + " dataset with method " +
                                         str(method) + "\nBest accuracy: " + str(round(accuracies[idx], 2)) + "%")

    # Show best metrics and accuracy
    print("Adjusted rand index =", np.round(randIndexs,4))
    print("Fowlkes Mallows =", np.round(fowlkes_mallows,4))
    print("Calinski-Harabasz score =", np.round(calinskis,4))
    print("Davies Bouldin =", np.round(davies,4))
    print("Best accuracy =", round(accuracies[idx], 4), ". Mean accuracy =", round(np.mean(accuracies), 4))


def mean_shift_execution(featuresPreprocessed, dataset, labelsTrue, save):
    # Choose a bandwidth according to the dataset
    bandwidth = 1.5
    if dataset == 'cmc':
        bandwidth = 1.75
    if dataset == 'zoo':
        bandwidth = 2
    if dataset == 'pen-based':
        bandwidth = 1

    # Execute algorithm
    m = MeanShiftUB.MeanShiftUB(dataset=featuresPreprocessed, bandwidth=bandwidth)
    labelsPred = m.get_labels()

    # Compute the metrics
    randIndexs = validationMetrics.rand_index(labelsTrue, labelsPred)
    fowlkes_mallows = validationMetrics.fowlkes_mallows(labelsTrue, labelsPred)
    calinskis = validationMetrics.calinski_harabasz(featuresPreprocessed, labelsPred)
    davies = validationMetrics.davies_bouldin(featuresPreprocessed, labelsPred)

    print("Metrics:")
    print("\tAdjusted rand index =", randIndexs)
    print("\tFowlkes Mallows =", fowlkes_mallows)
    print("\tCalinski-Harabasz score =", calinskis)
    print("\tDavies Bouldin =", davies)
    print("\tBest K value =", len(m.get_cluster_centers()))

    # Plot heatmap
    numRealClasses = len(np.unique(labelsTrue))
    if numRealClasses == len(np.unique(labelsPred)):
        accuracy = validationMetrics.accuracy_labels(labelsTrue, labelsPred)
        print("\tAccuracy =", accuracy)
        distributions = validationMetrics.confussion_matrix_labels(labelsTrue, labelsPred)
        if save:
            check_and_create_folder("../results/")
            check_and_create_folder("../results/" + str(dataset) + "/")
        validationMetrics.plot_heatmap(distributions, size=(15, 15), save=save,
                                       path="../results/"+str(dataset)+"/"+"MS-Matrix_"+str(numRealClasses),
                                       title="Confussion matrix for " + str(dataset) + " dataset with method MS"+
                                             "\nAccuracy: " + str(round(accuracy, 2)) + "%")


def agglomerative_execution(featuresPreprocessed, dataset, num_clusters, labelsTrue, save):
    results_matrix = np.zeros((3, 3, 4, num_clusters[-1] - num_clusters[0] + 1))
    accuracies = []
    distributions = []
    numRealClasses = len(np.unique(labelsTrue))

    # Compute all the combinations of affinity and linkage
    for it, num_cluster in enumerate(num_clusters):
        print("Number of clusters:", num_cluster)
        for i, affinity in enumerate(["euclidean", "manhattan", "cosine"]):
            for j, linkage in enumerate(["complete", "average", "single"]):
                # Execute algorithm
                m = Agglomerative.Agglomerative(K=num_cluster, dataset=featuresPreprocessed,
                                                affinity=affinity, linkage=linkage)
                labelsPred = m.get_labels()

                # Compute the accuracy if the number of classes coincides
                if num_cluster == numRealClasses:
                    accuracies.append(validationMetrics.accuracy_labels(labelsTrue, labelsPred))
                    distributions.append(validationMetrics.confussion_matrix_labels(labelsTrue, labelsPred))

                # Metrics results
                results_matrix[i][j][0][it] = validationMetrics.rand_index(labelsTrue, labelsPred)
                results_matrix[i][j][1][it] = validationMetrics.fowlkes_mallows(labelsTrue, labelsPred)
                results_matrix[i][j][2][it] = validationMetrics.calinski_harabasz(featuresPreprocessed, labelsPred)
                results_matrix[i][j][3][it] = validationMetrics.davies_bouldin(featuresPreprocessed, labelsPred)

    # Plot all the results in the same plot
    fig, axs = plt.subplots(3, 3, figsize=(15, 15))
    for i, affinity in enumerate(["euclidean", "manhattan", "cosine"]):
        for j, linkage in enumerate(["complete", "average", "single"]):
            ax = axs[i][j]
            results_matrix[i][j][0] /= np.max(results_matrix[i][j][0])
            ax.plot(num_clusters, results_matrix[i][j][0], '.-', color='red', label="Adjusted rand index (max)")
            results_matrix[i][j][1] /= np.max(results_matrix[i][j][1])
            ax.plot(num_clusters, results_matrix[i][j][1], '.-', color='blue', label="Fowlkes Mallows (max)")
            results_matrix[i][j][2] /= np.max(results_matrix[i][j][2])
            ax.plot(num_clusters, results_matrix[i][j][2], '.-', color='green', label="Calinski-Harabasz score (max)")
            results_matrix[i][j][3] /= np.max(results_matrix[i][j][3])
            ax.plot(num_clusters, results_matrix[i][j][3], '.-', color='orange', label="Davies Bouldin (min)")
            ax.set_title("Affinity = " + affinity + ". Linkage = " + linkage)

    fig.suptitle("Comparison number of clusters of " + str(dataset) + " dataset with method AGG",
                 fontsize=25)
    if save:
        check_and_create_folder("../results/")
        check_and_create_folder("../results/" + str(dataset) + "/")
        path = "../results/" + str(dataset) + "/" + "AGG-" + str(num_clusters[0]) + "_" + str(num_clusters[-1])
        plt.savefig(path, dpi=200)
        print("Plots saved correctly at " + path[3:] + ".png")
    plt.show()

    # Plot all the heatmaps in the same plot
    n_clusters = ["Cluster " + str(value + 1) for value in range(numRealClasses)]
    fig, axs = plt.subplots(3, 3, figsize=(20, 20))
    for i, affinity in enumerate(["euclidean", "manhattan", "cosine"]):
        for j, linkage in enumerate(["complete", "average", "single"]):
            ax = axs[i][j]
            accuracy = round(accuracies[i + j * 3] * 100, 2)
            distribution = distributions[i + j * 3]
            dataframe = pd.DataFrame(distribution, columns=n_clusters, index=n_clusters)
            sns.heatmap(dataframe, annot=True, ax=ax, cmap="YlGnBu", square=True, annot_kws={"fontsize": 7})
            ax.set_title("Affinity = " + affinity + ".\nLinkage = " + linkage + ".\nAccuracy = " + str(accuracy) + "%")

    fig.suptitle("Confussion matrixs for " + str(dataset) + " dataset with method AGG", fontsize=25)
    if save:
        check_and_create_folder("../results/")
        check_and_create_folder("../results/" + str(dataset) + "/")
        path = "../results/" + str(dataset) + "/" + "AGG-Matrix_" + str(numRealClasses)
        plt.savefig(path, dpi=200)
        print("Heatmaps saved correctly at " + path[3:] + ".png")
    plt.show()


def check_and_create_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)
        folders = path.split("/")
        print("Folder "+folders[-2]+" created.")