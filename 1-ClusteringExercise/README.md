# Clustering exercise 
Introduction to Machine Learning (IML) - Master in Artificial Intelligence

## Team number 4

Composed by:
- Blanca Llauradó Crespo
- Sonia Rabanaque Rodríguez
- Armando Rodriguez Ramos
- Anna Sallés Rius

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.7.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```
you can check if dependencies were installed by running next
command,it should print list with installed dependencies
```bash
pip list
```
5. Close virtual env
```bash
deactivate
```
## Execute scripts
1. open virtual env
```bash
source venv/bin/activate
```
2. Running the script

   Any algorithm can be selected as well as its parameters.
   When executing main.py file as mentioned below, the terminal displays several 
   possible options depending on the dataset and the algorithm chosen.
   - First of all, the terminal will show up the possible datasets to choose. 
   Although it is possible to choose any of the datasets provided we strongly recommend
   to use the ones already analyzed in the report: {zoo, cmc, pen-based}. 
   - Secondly, it is possible to choose the algorithm wanted to compute the dataset. In
   this case the options are the following ones: {KM = K-Means, KM_SK = K-Means provided by Sklearn
   (only used if wanted to compare the results), MS = Mean Shift, 
   AGG = Agglomerative, KHM = K-Harmonic Means, BKM = Bisecting K-Means, 
   PCM = Possibilistic C-Means}.
   - Once the algorithm has been already chosen it will display the parameters that 
   are possible to change. If you want to execute with the default parameters you 
   only need to press 'Enter'.
   - Afterwards, while displaying 'Enter the range for the number of clusters' it is
   mandatory to choose a range where the real number of classes belongs to (the recommended ranges are already explicit in the report).
   - Finally, the display will ask you if you want to save the results computed. Select
   0 to refuse and 1 to agree.
 ```bash
 python3 main.py
 ```
3. Close virtual env
```bash
deactivate
```