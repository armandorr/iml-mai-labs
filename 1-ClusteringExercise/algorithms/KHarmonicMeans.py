import numpy as np
from metrics import distanceMetrics

class KHarmonicMeans:
    """
    Args:
        - K: (int) Number of clusters.
        - dataset: Data points or instances.
        - p (float): Parameter associated with the Lp distance calculation
        - max_iterations (int): Number of iterations that the algorithm does before finish (100 by default).
        - epsilon (float): Minimum distance among sample and centroid (used to sanitaze arrays)
        - delta (float): Stopping criteria for performance convergence
        - seed (float): Initial seed for performing random actions in a replicable way
        - validationClasses: Ground truth classes of the "dataset" instances.
        - plus_plus (bool): Indicates the initialization method used.
                - True for using the initialization of kmeans++
                - False for using the initialization of ordinary kmeans
        - verbose (bool): Indicates whether to show or not the evolution of the algorithm
    """
    def __init__(self, K, dataset, p=3, max_iterations=100, epsilon=1e-10, delta=1e-4, seed=None, validationClasses=None, plus_plus=True, verbose=False):
        self.K = K
        self.dataset = dataset
        self.p = p
        self.max_iterations = max_iterations
        self.verbose = verbose
        if seed:
            np.random.seed(seed)

        self.epsilon = epsilon
        self.delta = delta

        self.centroids = []
        self.samples = []
        self.labels = []
        self.performance = []
        self.membership = []

        # Select the way to initialize clusters
        if plus_plus:
            self.init_centroids_kmeans_plus_plus()
        else:
            self.init_centroids()

        self.executeAlgorithm()

        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    def init_centroids(self):
        # Ordinary kmeans initialization
        centroids = []
        for i in range(self.K):
            centroids.append(self.dataset.values[np.random.randint(0, len(self.dataset.values) - 1)])
        self.centroids = centroids


    def init_centroids_kmeans_plus_plus(self):
        # Kmeans++ initialization
        centroids = []

        # First point
        centroid_first_id = np.random.randint(1, len(self.dataset.values))
        centroids.append(self.dataset.values[centroid_first_id-1])

        #Select the remaining points
        for i in range(self.K - 1):
            distances = []
            for center in centroids:
                dists = distanceMetrics.minkowski(2, self.dataset.values, center)
                distances.append(dists)

            minDistances = np.min(distances,axis=0)
            distance = (minDistances**2)/np.sum((minDistances**2))
            index = list(range(len(self.dataset.values)))
            new_centroid_id = np.random.choice(index, size=1, p=distance)
            centroids.append(self.dataset.values[new_centroid_id])

        self.centroids = centroids

    def get_samples(self):
        return self.samples

    def get_labels(self):
        return self.labels

    def get_membership(self):
        return self.membership

    def get_centroids(self):
        return self.centroids

    def get_performance(self):
        return self.performance

    def get_distributions(self):
        assert self.distributions, "Distributions not defined, use the validationClasses param"
        return self.distributions

    def recompute_centroids(self):
        # Recompute the centroids of the method
        distances, membership = self.compute_memberships(self.dataset.values)
        weights = np.sum(distances ** (-self.p - 2), axis=0) / (np.sum(distances ** (-self.p), axis=0) ** 2)

        newCentroids = []
        for i in range(len(self.centroids)):
            memberWeighted = membership[i] * weights
            numerator = np.sum(memberWeighted * self.dataset.values.T, axis=1)
            denominator = np.sum(memberWeighted)
            newCentroids.append(numerator / denominator)

        self.membership = membership

        return newCentroids

    def compute_memberships(self, dataset):
        # Compute the membership matrix
        distances = []
        for centroid in self.centroids:
            distances.append(np.maximum(distanceMetrics.minkowski(2, dataset, centroid), self.epsilon))

        distances = np.array(distances)
        distancesPowered = distances ** (-self.p - 2)
        denominator = np.sum(distancesPowered, axis=0)
        membership = distancesPowered/denominator

        return distances, membership

    def executeAlgorithm(self):
        # Main loop of the algorithm
        lastPerformanceValue = -1
        performanceValue = -1
        iteration = 0
        # Check the stopping conditions
        while (iteration <= self.max_iterations and
               abs(performanceValue - lastPerformanceValue) > self.delta) or \
                lastPerformanceValue == -1:

            self.centroids = self.recompute_centroids()
            lastPerformanceValue = performanceValue
            performanceValue = self.performanceFunction()
            self.performance.append(performanceValue)
            iteration += 1
            if self.verbose: print("Iteration", iteration, ". Performance:", performanceValue)

        self.assignSamples()

    def assignSamples(self):
        # Assign each sample to a cluster in a crip way (for validation purposes)
        _, membership = self.compute_memberships(self.dataset.values)

        self.samples = []
        self.labels = np.argmax(membership.T,axis=1)
        for clusterIdx in range(self.K):
            samples = [idx for idx, c in enumerate(self.labels) if c == clusterIdx]
            self.samples.append(samples)

    def performanceFunction(self):
        # Computes the performance function at the specific moment
        values = [0] * len(self.dataset.values)
        for centroid in self.centroids:
            values += 1 / ((distanceMetrics.minkowski(2, self.dataset.values, centroid)) ** self.p)
        return np.sum(self.K / values)

    def validate(self, validationClasses):
        # Compute the class validation that results in a confusion matrix
        _, membership = self.compute_memberships(self.dataset.values)

        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}
        distributions = np.zeros((self.K, self.K))
        for idx, row in enumerate(self.dataset.values):
            clusterIdx = np.argmax(membership.T[idx])
            distributions[clusterIdx][dict_T[validationClasses[idx]]] += 1

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster, position):
            cluster_copy = np.array(cluster.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < self.K:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered
