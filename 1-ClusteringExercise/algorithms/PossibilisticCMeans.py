import numpy as np
from metrics import distanceMetrics
from algorithms import KHarmonicMeans


class PossibilisticCMeans:
    """
    Args:
        - C: (int) Number of clusters
        - dataset: Data points or instances.
        - m: fuzziness of the final possibilistic K-Partition.
             When m --> 1 the membership function is hard, and when m --> inf, the memberships are maximally fuzzy.
        - p (float): Parameter associated with the Lp distance calculation
        - epsilon (float): Stopping criteria for performance convergence
        - max_iterations (int): Number of iterations that the algorithm does before finish (100 by default).
        - seed (float): Initial seed for performing random actions in a replicable way
        - validationClasses: Ground truth classes of the "dataset" instances.
        - verbose (bool): Indicates whether to show or not the evolution of the algorithm
    """

    def __init__(self, C, dataset, m=2, p=3, epsilon=1e-4, max_iterations=100, seed=None, validationClasses=None, verbose=False):
        assert 1 < m < np.inf, "Not correct value for m"
        self.C = C
        self.dataset = dataset
        self.m = m
        self.epsilon = epsilon
        if seed:
            np.random.seed(seed)
        self.verbose = verbose

        self.samples = []
        self.labels = []
        self.performance = []

        # Initialize with k-harmonic means
        khm = KHarmonicMeans.KHarmonicMeans(K=C, p=p, dataset=dataset, seed=seed, verbose=False)
        self.centroids = khm.get_centroids()
        self.U = khm.get_membership()

        self.n = None
        self.iterations = 0
        self.max_iterations = max_iterations

        # Initialize universe
        self.init_universe()

        # Execute algorithm
        self.execute()

        # Perform validation
        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    def init_universe(self):
        # Compute distances and n array
        distances = self.compute_distances()
        U_m = self.U ** self.m
        self.n = np.sum(U_m * distances, axis=1) / np.sum(U_m, axis=1)

        # Restore universe (U) possibilities
        self.U = np.zeros((self.C, len(self.dataset.values)))

    def get_centroids(self):
        return self.centroids

    def get_samples(self):
        return self.samples

    def get_labels(self):
        return self.labels

    def get_performance(self):
        return self.performance

    def get_distributions(self):
        assert self.distributions, "Distributions not defined, use the validationClasses param"
        return self.distributions

    def compute_distances(self):
        # Compute the distances from samples to clusters
        distances = []
        for centroid in self.centroids:
            distances.append(distanceMetrics.minkowski(2, self.dataset.values, centroid))
        return np.array(distances)

    def recompute_U(self, distances2):
        # Recompute universe matrix
        return 1 / (1 + np.divide(distances2.T, self.n).T ** (1 / (self.m - 1)))

    def execute(self):
        # Main loop of the algorithm
        lastU = None
        # Check the stopping conditions
        while (self.iterations == 0) or \
                (self.iterations < self.max_iterations and
                 np.linalg.norm(self.U - lastU) > self.epsilon):

            if self.verbose:
                print("Iteration number", self.iterations)

            # Compute distances and square them
            distances = self.compute_distances()
            distances2 = distances ** 2

            # Recompute universe U
            lastU = self.U.copy()
            self.U = self.recompute_U(distances2)

            # Update cluster centroids
            U_m = self.U ** self.m
            self.centroids = np.divide(np.matmul(U_m, self.dataset.values).T, np.sum(U_m, axis=1)).T

            self.iterations += 1

            # Retrieve the performance function
            performance_value = self.performanceFunction(distances2)
            self.performance.append(performance_value)
            if self.verbose:
                print("Performance:", performance_value)

        # Finalize assigning the samples to the most possible cluster
        self.assignSamples()

    def performanceFunction(self, distances2):
        # Performance function of Possibilistic C-Means
        J = np.sum((self.U ** self.m * distances2)) + np.matmul(self.n, np.sum((1 - self.U) ** self.m, axis=1))
        return J

    def assignSamples(self):
        # Assign each sample to a cluster in a crip way (for validation purposes)
        self.samples = []
        self.labels = np.argmax(self.U.T, axis=1)

        for clusterIdx in range(self.C):
            samples = [idx for idx, c in enumerate(self.labels) if c == clusterIdx]
            self.samples.append(samples)

    def validate(self, validationClasses):
        # Compute the class validation that results in a confusion matrix
        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}

        distributions = np.zeros((self.C, self.C))
        for idx, row in enumerate(self.dataset.values):
            clusterIdx = np.argmax(self.U.T[idx])
            distributions[clusterIdx][dict_T[validationClasses[idx]]] += 1

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster, position):
            cluster_copy = np.array(cluster.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < self.C:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered
