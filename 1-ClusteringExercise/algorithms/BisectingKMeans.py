import numpy as np
from metrics import distanceMetrics
from algorithms import Cluster, KMeansUB


class BisectingKMeans:
    """
    Args:
        - K: (int) Number of clusters.
        - dataset: Data points or instances.
        - iterations (int): Number of iterations to apply the Bisecting Step.
        - cluster_to_split (str): Method to choose the cluster to split.
                - 'biggest' (default): Take the cluster with more instances.
                - 'least_similarity': Take the more heterogeneous cluster, the one with lowest overall similarity.
                - 'combination': Combination of the previous ones. For the N more heterogeneous clusters, take the one with more instances.
        - max_iterations (int): Number of maximum iterations for the basic K-Means algorithm (25 by default).
        - distance (str): Distance function used to assign instances to cluster centroids.
                - 'Minkowski' (default): Minkowski distance metric.
                - 'Cosine': Cosine distance (from cosine similarity).
        - p (float): Exponent used to compute Minkowski distance.
        - seed (float): Initial seed for performing random actions in a replicable way.
        - validationClasses: Ground truth classes of the "dataset" instances.
    """

    def __init__(self, K, dataset, iterations=10, cluster_to_split='biggest', max_iterations=25, distance='Minkowski', p=2, seed=None, validationClasses=None, verbose=False):

        # Bisecting K-Means parameters
        self.K = K
        self.dataset = dataset
        self.verbose = verbose
        self.iterations = iterations
        self.clusters = []
        self.labels = []
        self.performance = []

        # K-Means parameters
        self.distance = distance
        self.p = p
        self.seed = seed
        self.max_iterations = max_iterations

        # Select cluster to split method
        assert cluster_to_split in ['biggest', 'least_similarity', 'combination'], "The cluster to split method name \""+cluster_to_split+"\" is not correct"
        if cluster_to_split == 'biggest':
            self.cluster_to_split_selection = self.pick_biggest_cluster
        elif cluster_to_split == 'least_similarity':
            self.cluster_to_split_selection = self.pick_least_similarity_cluster
        else:
            self.cluster_to_split_selection = self.pick_combination_cluster

        # Set initial cluster and execute the algorithm
        self.init_cluster(dataset)
        self.execute()
        self.assignLabels()

        # Validate results (if needed)
        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    def init_cluster(self, dataset):

        # Initial cluster that contains all the instances
        initialCluster = Cluster.Cluster(dataset, [])
        initialCluster.set_samples(np.arange(len(dataset)))
        self.clusters.append(initialCluster)

    def get_clusters(self):
        return self.clusters

    def get_labels(self):
        return self.labels

    def get_performance(self):
        return self.performance

    def get_distributions(self):
        assert self.distributions, "Distributions not defined, use the validationClasses param"
        return self.distributions

    def execute(self):

        # Until K clusters, select cluster to split and apply Bisecting Step
        while len(self.clusters) < self.K:
            if self.verbose: print('BISECTING K-MEANS:', len(self.clusters), 'CLUSTERS:')
            clusterToSplit, idx = self.cluster_to_split_selection()
            subclusters = self.bisectingStep(clusterToSplit)
            self.update_clusters(idx, subclusters)

            self.performance.append(self.performanceFunction())

    def update_clusters(self, idxClusterToRemove, clustersToAdd):

        # Remove splitted cluster and add sub-clusters
        self.clusters.pop(idxClusterToRemove)
        self.clusters += clustersToAdd

    def bisectingStep(self, clusterToSplit):

        max_overall_similarity = 0
        subclusters = None
        for i in range(self.iterations):

            # Divide the cluster in 2 subclusters applying basic K-Means
            if self.verbose: print('    Iteration', i, 'of', self.iterations)
            kmeans = KMeansUB.KMeansUB(2, self.dataset.iloc[clusterToSplit.samples], max_iterations=self.max_iterations,
                                       distance=self.distance, p=self.p, seed=self.seed, kmeans_plus_plus=False)

            # If best partition (higher overall similarity), keep it
            overall_similarity = np.sum([cluster.get_overall_similarity() for cluster in kmeans.clusters])
            if overall_similarity > max_overall_similarity:
                max_overall_similarity = overall_similarity

                # Subclusters respect the whole dataset
                subcluster1 = Cluster.Cluster(self.dataset, kmeans.clusters[0].centroid)
                subcluster1.set_samples(clusterToSplit.samples[kmeans.clusters[0].samples])

                subcluster2 = Cluster.Cluster(self.dataset, kmeans.clusters[1].centroid)
                subcluster2.set_samples(clusterToSplit.samples[kmeans.clusters[1].samples])

                subclusters = [subcluster1, subcluster2]

        return subclusters

    def pick_biggest_cluster(self):

        # Take the cluster with more samples
        size = [len(cluster.samples) for cluster in self.clusters]
        cluster_id = np.argmax(size)

        return self.clusters[cluster_id], cluster_id

    def pick_least_similarity_cluster(self):

        # Take the cluster with low overall similarity
        similarities = [cluster.get_overall_similarity() for cluster in self.clusters]
        cluster_id = np.argmin(similarities)

        return self.clusters[cluster_id], cluster_id

    def pick_combination_cluster(self, N=3):

        # N less similar clusters (lower overall similarity)
        similarities = [cluster.get_overall_similarity() for cluster in self.clusters]
        similar_idxs = np.argsort(similarities)[:N]
        less_similar_clusters = [self.clusters[idx] for idx in similar_idxs]

        # Largest cluster of the previous (more samples)
        sizes = [len(cluster.samples) for cluster in less_similar_clusters]
        size_idx = np.argmax(sizes)
        cluster = less_similar_clusters[size_idx]

        return cluster, similar_idxs[size_idx]

    def assignLabels(self):

        # Assign each sample to a cluster (for validation purposes)
        self.labels = np.zeros(len(self.dataset))
        for clusterIdx, cluster in enumerate(self.clusters):
            indices = cluster.get_samples()
            self.labels[indices] = clusterIdx

    def performanceFunction(self):

        # Compute the performance function: Sum of Square Errors (SSE)
        J = 0
        for cluster in self.clusters:
            centroid = cluster.centroid
            indices = cluster.get_samples()
            J += np.sum((distanceMetrics.minkowski(2, self.dataset.iloc[indices], centroid)))
        return J

    def validate(self, validationClasses):
        # Compute the class validation that results in a confusion matrix

        distributions = np.zeros((self.K, self.K))
        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}
        for idx, sample in enumerate(self.dataset.values):
            if self.verbose: print("Cluster " + str(idx) + ":")
            distributions[int(self.labels[idx])][dict_T[validationClasses[idx]]] += 1
            if self.verbose: print("\t" + str(distributions))

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster_distribution, position):
            cluster_copy = np.array(cluster_distribution.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < self.K:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered