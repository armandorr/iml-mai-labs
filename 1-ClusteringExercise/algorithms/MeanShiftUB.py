import numpy as np
from sklearn.cluster import MeanShift


class MeanShiftUB:

    """
    Args:
        - dataset: Data points or instances.
        - bandwidth: Bandwidth used in the RBF kernel. If not used Sklean tries to estimate it using
                     sklearn.cluster.estimate_bandwidth
        - max_iter: Maximum number of iterations, per seed point before the clustering operation terminates
                    (300 by default) if it has not converged yet
         - validationClasses: Ground truth classes of the "dataset" instances.

    """

    def __init__(self, dataset, bandwidth=None, max_iter=300, validationClasses=None, verbose=False):
        self.dataset = dataset
        self.iter = max_iter
        self.bandwidth = bandwidth
        self.clusters = []
        self.verbose = verbose

        # Execute algorithm
        self.model = self.execute()

        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    def execute(self):
        clustering = MeanShift(bandwidth=self.bandwidth).fit(self.dataset.values)
        return clustering

    def get_labels(self):
        return self.model.labels_

    def get_cluster_centers(self):
        return self.model.cluster_centers_

    def get_distributions(self):
        return self.distributions

    def validate(self, validationClasses):
        K = len(np.unique(self.model.labels_))
        distributions = np.zeros((K,K))
        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}
        for idx, labelPred in enumerate(self.model.labels_):
            if self.verbose: print("Cluster "+str(idx)+":")
            distributions[labelPred][dict_T[validationClasses[idx]]] += 1
            if self.verbose: print("\t"+str(distributions))

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster_distribution, position):
            cluster_copy = np.array(cluster_distribution.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < K:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered