import numpy as np
from algorithms import Cluster
from metrics import distanceMetrics

class KMeansUB:
    """
    Args:
        - K: (int) Number of clusters (2 by default).
        - dataset: Data points or instances.
        - max_iterations: Number of iterations that the algorithm does before finish (25 by default).
        - distance (stc): Distance function used to assign instances to cluster centroids.
                - 'Minkowski' (default): Minkowski distance metric.
                - 'Cosine': Cosine distance (from cosine similarity).
        - p (float): Exponent used to compute Minkowski distance.
        - validationClasses: Ground truth classes of the "dataset" instances.
        - kmeans_plus_plus (bool): Indicates the initialization method used.
                - True if kmeans_plus_plus
                - False if ordinary kmeans
    """

    def __init__(self, K=2, dataset=None, max_iterations=25, distance='Minkowski', p=2, seed=None,
                 validationClasses=None, kmeans_plus_plus=True, verbose=False):
        self.K = K
        self.dataset = dataset
        self.clusters = []
        if seed:
            np.random.seed(seed)
        self.max_iterations = max_iterations
        self.verbose = verbose
        self.labels = []
        self.performance = []

        assert distance in ['Minkowski', 'Cosine'], "The distance name \""+distance+"\" is not correct"
        if distance == 'Minkowski':
            self.distance_function = lambda s, c: distanceMetrics.minkowski(p, s, c)
        elif distance == 'Cosine':
            self.distance_function = lambda s, c: distanceMetrics.cosine(s, c)

        # Select the way to initialize clusters
        if kmeans_plus_plus:
            self.init_centroids_kmeans_plus_plus()
        else:
            self.init_centroids()

        # Execute algorithm
        self.execute()

        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    def init_centroids(self):
        # Ordinary kmeans initialization
        centroids = []
        for i in range(self.K):
            centroids.append(self.dataset.values[np.random.randint(0, len(self.dataset.values) - 1)])
        self.clusters = list(map(lambda x: Cluster.Cluster(self.dataset, x), centroids))

    # Kmeans++ initialization
    def init_centroids_kmeans_plus_plus(self):
        centroids = []

        # First point
        centroid_first_id = np.random.randint(1, len(self.dataset.values))
        centroids.append(self.dataset.values[centroid_first_id-1])

        #Select the remaining points with an specified probability depending on the shortest distance from an instance
        # to its closest center already chosen
        for i in range(self.K - 1):
            distances = []
            for center in centroids:
                dists = self.distance_function(self.dataset.values, center)
                distances.append(dists)

            minDistances = np.min(distances, axis=0)
            distance = (minDistances**2)/np.sum((minDistances**2))
            index = list(range(len(self.dataset.values)))
            new_centroid_id = np.random.choice(index, size=1, p=distance)
            centroids.append(self.dataset.values[new_centroid_id])

        self.clusters = list(map(lambda x: Cluster.Cluster(self.dataset, x), centroids))

    # Main loop of the algorithm
    def execute(self):
        for i in range(self.max_iterations):
            if self.verbose:
                print("Iteration", i, "out of", self.max_iterations)
            # Assigning each point to its closest centroid
            equal = self.assign_samples()
            # Recomputing centroids
            self.recompute_centroids()
            if equal:
                break

            variances = list(map(lambda val: round(val, 2), self.get_variance()))
            total_variance = np.sum(variances)
            if self.verbose:
                print("Clusters variance:", variances, ", Total variance:", round(total_variance, 2))

    def get_variance(self):
        clusters_variance = []
        for cluster in self.clusters:
            clusters_variance.append(cluster.get_variance())

        return clusters_variance

    def get_clusters(self):
        return self.clusters

    def get_labels(self):
        return self.labels

    def get_performance(self):
        return self.performance

    def get_distributions(self):
        assert self.distributions, "Distributions not defined, use the validationClasses param"
        return self.distributions

    def get_centroids(self):
        centroids = []
        for cluster in self.clusters:
            centroids.append(cluster.get_centroid())
        return centroids

    def recompute_centroids(self):
        for cluster in self.clusters:
            cluster.recompute_centroid()

    # Assign each sample to a cluster
    def assign_samples(self):
        distances = []
        for cluster in self.clusters:
            distances.append(self.distance_function(self.dataset.values, cluster.get_centroid()))
        # Take minimal distance between a point and a centroid
        self.labels = np.argmin(distances, axis=0)

        all_clusters_equal = True
        some_old_samples = False
        for cluster_id, cluster in enumerate(self.clusters):
            cluster_samples = np.where(self.labels == cluster_id)[0]
            old_samples = cluster.get_samples()
            cluster.set_samples(cluster_samples)

            some_old_samples = some_old_samples or (old_samples != [])
            if old_samples != []:
                all_clusters_equal = all_clusters_equal and np.array_equal(old_samples, cluster_samples)
        # Check if the centroids have changed
        return some_old_samples and all_clusters_equal

    # Compute the class validation that results in a confusion matrix
    def validate(self, validationClasses):
        distributions = np.zeros((self.K, self.K))
        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}

        for idx, sample in enumerate(self.dataset.values):
            if self.verbose: print("Cluster " + str(idx) + ":")
            distributions[self.labels[idx]][dict_T[validationClasses[idx]]] += 1
            if self.verbose: print("\t"+str(distributions))

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster_distribution, position):
            cluster_copy = np.array(cluster_distribution.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < self.K:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered
