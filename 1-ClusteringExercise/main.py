from analyze import testDatasets

'''
STRATEGY:
Deal with missing values
- Numerical: replace by the mean (or mode in some cases)
- Categorical: create a new category "Other" or replace by the mode
    - Native-country: replace ? by United-States
    - Occupation: replace ? by "No information"
    - Workclass: replace ? by Private

    - If the difference of the most frequent class and the second one (the second class most frequent) 
      is bigger than X=20% (variable), replace by the most frequent class. Otherwise, create a new category.

Deal with different types
For the categorical variables:
- Two possible outcomes: Label encoding
- More than two outcomes: One hot encoding

Deal with different ranges: Min-Max scaling for each column.
'''


def main(arguments):
    if arguments["dataset"] is not None:
        if arguments["dataset"] == 'pen-based':
            im, enc, sca = False, False, True
        elif arguments["dataset"] == 'zoo':
            im, enc, sca = False, True, True
        elif arguments["dataset"] == 'cmc':
            im, enc, sca = False, True, True
        else:
            im, enc, sca = True, True, True

        if arguments["method"] is not None:
            if arguments["rangeK"] is not None:
                K = [int(val) for val in arguments["rangeK"].split(" ")]
            else:
                K = None
            testDatasets.executing_and_testing_dataset(dataset=arguments["dataset"], method=arguments["method"], rangeK=K,
                                                       impute=im, encode=enc, scale=sca, save=arguments["save"], options=arguments["opt"])


def get_KM_params():
    values = {"distance": 'Minkowski', "p": 2, "iter": 25, "initialization": True}
    dist = input("Enter the distance you want to use.\n"
                 "Minkowski by default. Options are {Minkowski and Cosine}: ")
    if dist == 'Cosine':
        values["distance"] = 'Cosine'
    else:
        pvalue = input("Enter p value for L-norm (2 by default):")
        if pvalue != "":
            while float(pvalue) <= 0:
                pvalue = input("Enter a CORRECT p value for L-norm (p>0):")
            values["p"] = float(pvalue)
    iter = input("Enter number of iterations (25 by default): ")
    if iter != "":
        values["iter"] = int(iter)
    initialization = input("Enter the initialization chosen (kmeans++ by default):\n"
                           "'kmeans++' or 'kmeans'")
    if initialization == 'kmeans':
        values["initialization"] = False
    return values

def get_BKM_params():
    values = {"iterations": 10, "cluster_to_split": 'biggest', "distance":'Minkowski', "p":2}
    dist = input("Enter the distance you want to use.\n"
                 "Minkowski by default. Options are {Minkowski and Cosine}: ")
    if dist == 'Cosine':
        values["distance"] = 'Cosine'
    else:
        pvalue = input("Enter p value for L-norm (2 by default):")
        if pvalue != "":
            while float(pvalue) <= 0:
                pvalue = input("Enter a CORRECT p value for L-norm (p>0):")
            values["p"] = float(pvalue)
    iter = input("Enter number of iterations (10 by default): ")
    if iter != "":
        values["iterations"] = int(iter)
    clust = input("Enter the method to choose the cluster to split: \n"
                  "'biggest' (default): Take the cluster with more instances.\n"
                  "'least_similarity': Take the more heterogeneous cluster, the one with lowest overall similarity.\n"
                  "'combination': Combination of the previous ones. For the N more heterogeneous clusters, take the one with more instances.")
    if clust != "":
        values["cluster_to_split"] = clust
    return values

def get_KHM_params():
    values = {"p": 3}
    p = input("Enter (p) the parameter associated with the Lp distance calculation (3 by default):")
    if p != "":
        values["p"] = float(p)
    return values

def get_PCM_params():
    values = {"m":1.2, "p":3, "max_iterations": 100}
    mvalue = input("Enter fuzziness value (2 by default):")
    if mvalue != "":
        values["m"] = float(mvalue)
    pvalue = input("Enter (p) the parameter associated with the Lp distance calculation (3 by default):")
    if pvalue != "":
        values["p"]: float(pvalue)
    iter = input("Enter maximum number of iterations (100 by default):")
    if iter != "":
        values["max_iterations"] = int(iter)
    return values


if __name__ == '__main__':
    args = None
    values = []
    dataset = input("Enter the dataset name. Analyzed ones are {pen-based, zoo, cmc}: ")
    method = input("Enter the Algorithm to compute. Options are {KM, KM_SK, MS, AGG, KHM, BKM, PCM}: ")
    if method == 'KM':
        values = get_KM_params()
    elif method == 'BKM':
        values = get_BKM_params()
    elif method == 'PCM':
        values = get_PCM_params()
    elif method == 'KHM':
        values = get_KHM_params()
    if method != 'MS':
        K = input("Enter the range for the number of clusters (should include the true number of classes).\n"
                  "Example 2 5 would perform the test using between 2 and 5 (included) clusters: ")
        save = int(input("Do you want to save the result? Yes[1]/No[0]: ")) == 1
    else:
        K = None
        save = False

    args = {"dataset": dataset, "method": method, "rangeK": K, "save": save, "opt": values}
    main(args)
