import time
import sklearn
import numpy as np

from sklearn.manifold import TSNE
from algorithms import PCA, plotData
from analyze import clusteringAnalysis
from preprocessing import preprocessingUtils, dataUtils


def main():

    # Read dataset and preprocess
    datasetName = input("Enter the dataset name. Analyzed ones are {pen-based, zoo, cmc}: ")
    features, classes = dataUtils.readData("datasets/" + str(datasetName) + ".arff")
    scaleBool = int(input("\nDo you want to scale the data of the dataset? Yes[1] / No[0]: ")) == 1
    scaleMethod = 'MinMax'
    if scaleBool:
        response = input("\nWhich scaler do you want to use? Options are [MinMax, Standard] (MinMax by default): ")
        if response != "":
            scaleMethod = response
    featuresPreprocessed = preprocessingUtils.preprocessDataframe(features, impute=True, encode=True, verbose=False,
                                                                  scale=scaleBool, scaleMethod=scaleMethod)
    columns = featuresPreprocessed.columns.values
    colNums = list(range(len(columns) - 1))
    np.random.shuffle(colNums)

    # Save results
    saveBool = int(input("\nDo you want to save the results that will be generated? Yes[1] / No[0]: ")) == 1
    path = None
    if saveBool:
        path = "../results/" + datasetName + "/"
        print("Okay. Results will be saved at", path)

    # Plot original data
    originalBool = int(input("\nDo you want to plot the original data of the dataset? Yes[1] / No[0]: ")) == 1
    plotCols = [columns[colNums[0]], columns[colNums[1]]]
    plotColsInd = list(range(len(columns)))
    if originalBool:

        # Columns to plot
        plotColsText = input(f"\nChoose from 2 to 3 original columns to plot the original data.\n"
                             f"Options are the columns: [{', '.join(columns)}]\n"
                             f"Write the columns separated by a blank space as "
                             f"'{columns[colNums[0]]} {columns[colNums[1]]}'.\n"
                             f"The columns from above are the ones selected by default: ")
        if plotColsText != "":
            plotCols = plotColsText.split(" ")

        plotColsInd = np.where(np.isin(columns, plotCols))[0]

        # Plot original dataset
        plotData.plot(featuresPreprocessed.loc[:,plotCols].values, k=len(plotCols), plotCols=plotCols, classesValues=classes.values,
                      title="Some features from the original dataset", savePath=path,
                      fileName=datasetName + "_original")

    # Dimensionality reduction
    reductionBool = int(input("\nDo you want to reduce the dimensionality in the selected dataset? "
                              "Yes[1] / No[0]: ")) == 1
    reductionName = None
    inTSNE = False  # this variable will be needed later
    generalK = 3
    if reductionBool:

        # Dimensionality reduction method
        reductionMethod = int(input("\nWhich method to reduce the dimensionality do you want to use? PCA[0] / "
                                    "Feature Agglomeration[1] / t-SNE[2]: "))

        # Principal component analysis (PCA)
        if reductionMethod == 0:
            reductionName = "PCA"

            # Type of PCA
            pcaType = 0
            pcaTypeResponse = input("\nWhich PCA method do you want to compute? OurPCA[0] / SklearnPCA[1] / "
                                    "SklearnIncrementalPCA[2] (0 by default): ")
            if pcaTypeResponse != "":
                pcaType = int(pcaTypeResponse)

            # PCA choose k dimensionality reduction (number of components or variance)
            dimensionalityText = f"\nConsider that the preprocessed dataset has {len(columns)} columns.\n" + \
                                 (f"If a k <= 1 is chosen, k will be interpreted as the "
                                  f"variance explained.\n" if pcaType != 2 else "") + \
                                 f"Select the dimensionality reduction parameter k: "
            pcaK = float(input(dimensionalityText))
            while pcaK > len(columns) or (pcaK < 0 if pcaType != 2 else pcaK < 1):
                pcaK = float(input(f"\nPlease introduce a valid value of k, {0 if pcaType != 2 else 1}"
                                   f" <= k <= {len(columns)}.\n"
                                   f"Select the dimensionality reduction parameter k: "))
            if pcaK >= 1:
                pcaK = int(pcaK)
            generalK = pcaK

            # Compute PCA: ours, sklearn or incremental PCA
            print("\nComputing PCA...\n")
            time_start = time.time()
            if pcaType == 0:  # ours
                pca = PCA.PCA(featuresPreprocessed, k=pcaK, plotCols=plotCols, plotOriginalDataset=False, plotNewSubspace=False,
                              datasetName=datasetName, savePath=path)
                featuresTransformed = pca.get_transformedData()
                print(f'\nPCA: Time elapsed: {time.time() - time_start} seconds')
                pcaK = pca.get_k()
            elif pcaType == 1:  # sklearn
                pca = sklearn.decomposition.PCA(n_components=pcaK)
                featuresTransformed = pca.fit_transform(featuresPreprocessed)
                print(f'\nPCA sklearn: Time elapsed: {time.time() - time_start} seconds')
                pcaK = pca.n_components_
            else:  # incremental PCA (sklearn)
                incrementalPCA = sklearn.decomposition.IncrementalPCA(n_components=pcaK)
                featuresTransformed = incrementalPCA.fit_transform(featuresPreprocessed)
                print(f'\nPCA incremental: Time elapsed: {time.time() - time_start} seconds')
                pcaK = incrementalPCA.n_components_

            # Plot PCA subspace
            title = "New subspace " + ("incremental" if pcaType == 2 else "") + "PCA " + \
                    ("(ours)" if pcaType == 0 else "(Sklearn)") + " with " + datasetName + " dataset"
            plotData.plot(featuresTransformed, k=pcaK, classesValues=classes.values,
                          title=title, savePath=path, fileName=datasetName + "_" + reductionName)

            # Final result
            print(f"\nPCA computed correctly with {pcaK} components")

        # Feature Agglomeration
        elif reductionMethod == 1:
            reductionName = "FA"

            # FA choose k dimensionality reduction
            faK = float(input(f"\nConsider that the preprocessed dataset has {len(columns)} columns.\n"
                            f"Select the dimensionality reduction parameter k (we highly recommend to use 3 in order to show the results): "))
            while faK > len(columns) or faK < 1:
                faK = float(input(f"\nPlease introduce a valid value of k, 1 <= k <= {len(columns)}.\n"
                                f"Select the dimensionality reduction parameter k: "))
            faK = int(faK)
            generalK = faK

            # Affinity and linkage values
            affinity = 'euclidean'
            affinityResponse = input("\nEnter the distance (affinity) you want to use.\n"
                                "Options are {euclidean and cosine} (euclidean by default): ")
            if affinityResponse != "":
                affinity = affinityResponse.lower()

            linkage = 'average'
            linkageResponse = input("\nEnter the linkage you want to use.\n"
                                    "Options are {average, complete and single} (average by default): ")
            if linkageResponse != "":
                linkage = linkageResponse.lower()

            # Compute Feature Agglomeration
            print("\nComputing Feature Agglomeration...\n")
            time_start = time.time()
            fa = sklearn.cluster.FeatureAgglomeration(n_clusters=faK, linkage=linkage, affinity=affinity)
            featuresTransformed = fa.fit_transform(featuresPreprocessed)
            print('Feature Agglomeration: Time elapsed: {} seconds'.format(time.time() - time_start))

            # Plot FA subspace
            plotData.plot(featuresTransformed, k=faK, title="New subspace FA (Sklearn)",
                          classesValues=classes.values, savePath=path, fileName=datasetName + "_" + reductionName)

            # Final result
            print(f"\nFeature Agglomeration computed correctly with {faK} components")

        # t-SNE
        else:
            reductionName = "TSNE"
            inTSNE = True

            # T-SNE choose k dimensionality reduction
            dim = 2
            dimResponse = input(f"\nSelect the dimensionality reduction parameter k: "
                                f"Options are 2 or 3 (2 by default): ")
            if dimResponse != "":
                dim = int(dimResponse)
            generalK = dim

            # Compute t-SNE
            print("\nComputing t-SNE...\n")
            time_start = time.time()
            featuresTransformed = TSNE(n_components=dim, random_state=1000,
                                       learning_rate='auto', init='pca').fit_transform(featuresPreprocessed)
            print('t-SNE: Time elapsed: {} seconds'.format(time.time() - time_start))

            # Plot t-SNE subspace
            plotData.plot(featuresTransformed, k=dim, classesValues=classes.values,
                          title="New subspace t-SNE", savePath=path, fileName=datasetName + "_" + reductionName)

            # Final result
            print(f"\nT-SNE computed correctly with {dim} components")

    # Clustering
    clusteringBool = int(input("\nDo you want to compute any clustering algorithm to the data? "
                               "Yes[1] / No[0]: ")) == 1
    if clusteringBool:

        # Clustering method and parameters
        clusteringMethod = int(input("\nWhich clustering method do you want to use? K-Means[0] /"
                                     " Agglomerative Clustering[1]: "))
        clusteringName = ""
        if clusteringMethod == 0:   # K-Means
            values = get_KM_params()
            clusteringName = 'K-Means'
        else:                       # AGG
            values = get_AGG_params()
            clusteringName = 'AGG'

        # Number of clusters (range or specific value)
        useRangeK = int(input(f"\nDo you want to test a range of clusters? Yes[1] / No[0]: ")) == 1
        if useRangeK:
            rangeK = input(
                f"\nEnter the range for the number of clusters, it should include the true number of classes "
                f"({len(np.unique(classes))}).\n"
                "Example 2 5 would perform the test using between 2 and 5 (included) clusters: ")
        else:
            rangeK = int(input("\nEnter the desired number of clusters: "))
            rangeK = str(rangeK) + " " + str(rangeK)

        # Clustering without t-sne
        if not inTSNE:
            datasetValues = featuresTransformed if reductionBool else featuresPreprocessed.values[:,plotColsInd]

            print(f"\nComputing {clusteringName}...\n")
            time_start = time.time()
            _, _, _, labels = clusteringAnalysis.applyClustering(datasetName=datasetName,
                                                                 datasetValues=datasetValues, classes=classes,
                                                                 methodValue=clusteringMethod, onlyOneK=not useRangeK,
                                                                 rangeK=rangeK, save=saveBool, options=values)
            print(f'{clusteringName}: Time elapsed: {time.time() - time_start} seconds')

            filename = datasetName + ("_" + reductionName if reductionBool else "") + "_" + \
                       ("KM" if clusteringMethod == 0 else "AGG")

            # Number of clusters (to choose from the obtained results if test range)
            numKshow = int(rangeK[0])
            if useRangeK:
                numKshow = int(input("\nConsidering the previous validation metrics, enter the desired number of clusters: "))

            # Plot clustered data
            plotData.plot(datasetValues, k=generalK if reductionBool else len(plotCols),
                          classesValues=labels[numKshow - int(rangeK[0])],
                          title="Subspace with clustering", savePath=path,
                          fileName=filename)

        # Clustering with t-sne
        else:

            # Set N random seeds and initialize
            seeds = [7296, 15994, 59377, 6918, 29889, 27115, 17736, 7136, 75369, 20987]
            accuracies, randIndexs, fowlkes_mallows, calinskis, davies, distributions = [], [], [], [], [], []
            times = []

            # Traverse the seeds and compute the T-SNE
            for seed in seeds:

                # Compute t-SNE
                time_start = time.time()
                tsne = TSNE(n_components=dim, random_state=seed, learning_rate='auto', init='pca')
                featuresTransformed = tsne.fit_transform(featuresPreprocessed)
                times.append(time.time() - time_start)

                # Clustering
                accuracies_aux, metrics_aux, distributions_aux, _ = clusteringAnalysis.applyClustering(
                    datasetName=datasetName, datasetValues=featuresTransformed, classes=classes,
                    methodValue=clusteringMethod, rangeK=rangeK, save=saveBool,
                    options=values, onlyOneK=not useRangeK, verbose=False)

                # Save obtained results
                accuracies.append(accuracies_aux)
                randIndexs.append(metrics_aux[0])
                fowlkes_mallows.append(metrics_aux[1])
                calinskis.append(metrics_aux[2])
                davies.append(metrics_aux[3])
                distributions.append(distributions_aux)

            # Compute mean values
            final_acc = np.sum(accuracies, axis=0) / len(seeds)
            final_ri = np.sum(randIndexs, axis=0) / len(seeds)
            final_fm = np.sum(fowlkes_mallows, axis=0) / len(seeds)
            final_c = np.sum(calinskis, axis=0) / len(seeds)
            final_d = np.sum(davies, axis=0) / len(seeds)
            final_dist = np.sum(distributions, axis=0) / len(seeds)

            # Print mean values
            print("Mean accuracy using t-SNE", np.sum(final_acc) / len(final_acc))
            print("Rand Index using t-SNE", final_ri)
            print("Fowlkes_mallows using t-SNE", final_fm)
            print("Calinski using t-SNE", final_c)
            print("Davies using t-SNE", final_d)
            print("Times t-SNE", times)
            print("Times mean t-SNE", np.sum(times) / len(times))

            # Plot results
            if useRangeK:

                # Metrics for each k value
                clusteringAnalysis.plot_figures(final_acc, final_ri, final_fm, final_c, final_d,
                                                datasetName=datasetName, save=False, methodValue=clusteringMethod,
                                                rangeK=rangeK, distributions=final_dist, classes=classes)
            else:

                # Apply t-SNE
                tsne = TSNE(n_components=int(dim), random_state=1000, learning_rate='auto', init='pca')
                featuresTransformed = tsne.fit_transform(featuresPreprocessed)

                # Apply clustering
                print(f"\nComputing {clusteringName}...\n")
                time_start = time.time()
                _, metrics_aux, _, labels = clusteringAnalysis.applyClustering(datasetName=datasetName,
                                                                               datasetValues=featuresTransformed,
                                                                               classes=classes, verbose=False,
                                                                               methodValue=clusteringMethod,
                                                                               rangeK=rangeK, save=saveBool,
                                                                               options=values, onlyOneK=not useRangeK)
                print(f'{clusteringName}: Time elapsed: {time.time() - time_start} seconds')

                # Plot results
                filename = datasetName + ("_" + reductionName if reductionBool else "") + "_" \
                           + ("KM" if clusteringMethod == 0 else "AGG")

                plotData.plot(featuresTransformed, k=generalK if reductionBool else len(plotCols), classesValues=labels[0],
                              title="Subspace with clustering", savePath=path, fileName=filename)


def get_KM_params():
    values = {"distance": 'Minkowski', "p": 2, "iter": 25, "initialization": True}
    dist = input("\nSelect the distance that you want to use. Minkowski[0] / Cosine[1] (0 by default): ")
    if dist != '' and int(dist) != 0:
        values["distance"] = 'Cosine'
    else:
        pvalue = input("\nEnter the p value for the L-norm (2 by default): ")
        if pvalue != "":
            while float(pvalue) <= 0:
                pvalue = input("\nPlease enter a correct p value (p > 0) for the L-norm: ")
            values["p"] = float(pvalue)
    iter = input("\nEnter the number of iterations (25 by default): ")
    if iter != "":
        values["iter"] = int(iter)
    initialization = input("\nEnter the desired initialization. KMeans++[0] / KMeans[1] (0 by default): ")
    if initialization != '' and int(initialization) != 0:
        values["initialization"] = False

    return values


def get_AGG_params():
    values = {"affinity": 'euclidean', "linkage": 'average'}

    affinityResponse = input("\nEnter the distance (affinity) you want to use.\n"
                             "Options are {euclidean, manhattan and cosine} (euclidean by default): ")
    if affinityResponse != "":
        values["affinity"] = affinityResponse.lower()

    linkageResponse = input("\nEnter the linkage you want to use.\n"
                            "Options are {average, complete and single} (average by default): ")
    if linkageResponse != "":
        values["linkage"] = linkageResponse.lower()

    return values


if __name__ == '__main__':
    main()
