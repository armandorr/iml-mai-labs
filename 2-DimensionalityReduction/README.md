# Exercise number 2
### Dimensionality reduction with PCA and FeatureAgglomeration and Visualization using PCA and t-SNE
Introduction to Machine Learning (IML) - Master in Artificial Intelligence

## Team number 4

Composed by:
- Blanca Llauradó Crespo
- Sonia Rabanaque Rodríguez
- Armando Rodriguez Ramos
- Anna Sallés Rius

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.7.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```
you can check if dependencies were installed by running next
command,it should print list with installed dependencies
```bash
pip list
```
5. Close virtual env
```bash
deactivate
```
## Execute scripts
1. open virtual env
```bash
source venv/bin/activate
```
2. Running the script

   Any reduction technique and clustering algorithm can be selected as well as its parameters. When executing `main.py` file as mentioned below, the terminal displays several possible options depending on the options chosen.

   - First of all, the terminal will show the possible datasets to use. Although it is possible to choose any of the datasets provided, we strongly recommend to use the ones already analyzed in the report: {zoo, cmc, pen-based}.
   - Then, it is possible to choose if the data has to be scaled or not (select 0 to refuse and 1 to agree). If true, you can also select the scaler to apply to numerical values between the following: {MinMax, Standard}, where 'MinMax' is the default value.
   - After that, it is possible to decide if the user wants to save all the following generated results or not.
   - The display will ask you if you want to plot the original data. If you agree, then you have to choose the variables that you want to visualize (by indicating the name of the features) or you can select the default ones.
   - Afterwards, it is possible to choose if you want to apply reduction dimensionality to the dataset. If you agree, then you have to choose the technique to apply. In this case, the options are the following ones: {PCA, Feature Agglomeration, t-SNE}.
       - If Principal Components Analysis (PCA) option is selected, the display will allow you to select the PCA method: {OurPCA, SklearnPCA, SklearnIncremental}. Moreover, you have to introduce the number of connected components to reduce the dimensionality (if `k>1`) or the minimum variance explained (if `k <= 1`).
       - If the chosen approach is Feature Agglomeration (FA), you have to introduce the number of components to reduce the dataset and some of the parameters of the technique such as the affinity (between {euclidean, cosine}) and linkage (were options are {average, complete, single}).
       - In the case of t-SNE, as before, the dimensionality reduction parameter has to be specified to obtain the results.
   - Then, independently of applying dimensionality reduction or not, you can choose if you want to compute any clustering algorithm to the data (the original or the reduced one). If you agree, you can select the clustering method: {K-Means or Agglomerative Clustering}. Once the algorithm has been already chosen, it will display the parameters that are possible to change. If you want to execute it with the default parameters, you only need to press 'Enter'.
     - To select the number of clusters, there are two options: use a range of clusters or not. If the first one is taken, you have to introduce the desired range when requested. Otherwise, the exact number of clusters to use has to be indicated.

 ```bash
 python3 main.py
 ```
3. Close virtual env
```bash
deactivate
```