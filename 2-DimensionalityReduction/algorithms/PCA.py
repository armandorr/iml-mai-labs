import numpy as np
from algorithms import plotData
import pandas as pd


class PCA:
    """
    Args:
        - dataset (dataframe): Data points or instances.
        - k (int): The number of components to compute the PCA
        - plotCols (array): Original columns to be plotted, if None no plot is produced
        - plotNewSubspace (bool): True if we want to plot the new subspace created by the PCA
        - savePath (string): String that represent a path to save the images, if None, the plots will no be saved
        - datasetName (string): Name of the dataset to use it in the saving plots
    """

    def __init__(self, dataset, k, plotCols=None, plotOriginalDataset=False, plotNewSubspace=False, savePath=None, datasetName=None):
        self.dataset = dataset
        self.k = k  # if k < 1, use it as variance
        self.plotCols = plotCols
        self.plotOriginalDataset = plotOriginalDataset
        self.plotNewSubspace = plotNewSubspace
        self.path = savePath
        self.datasetName = datasetName
        assert (not self.path) or self.datasetName, \
            "If a save path is used you should also use the datasetName parameter"
        self.transformedData = None
        self.execute()

    def get_transformedData(self):
        return self.transformedData

    def get_k(self):
        return self.k

    def execute(self):
        # Pre-step
        print("Original dataset:")
        print(self.dataset)

        # Step 2: Plot the original data set (choose two or three of its features to visualize it)
        if self.plotOriginalDataset and self.plotCols:
            plotData.plot(data=self.dataset.loc[:, self.plotCols].values, k=len(self.plotCols),
                          plotCols=self.plotCols, title='Original data set', savePath=self.path,
                          fileName=self.datasetName+"_original_inPCA")

        # Step 3: Subtract mean
        OriginalMean = np.mean(self.dataset.values, axis=0)
        rowDataAdjust = np.subtract(self.dataset.values, OriginalMean)

        # Step 4: Covariance matrix
        covariance = np.cov(rowDataAdjust, rowvar=False)
        print("Covariance matrix:")
        print(covariance,end="\n\n")

        # Step 5: Eigenvectors and eigenvalues
        eigenvalues, eigenvectors = np.linalg.eig(covariance)
        eigenvalues, eigenvectors = np.real(eigenvalues), np.real(eigenvectors)
        print("All eigenvalues:")
        print(eigenvalues, end="\n\n")
        print("All eigenvectors:")
        print(eigenvectors, end="\n\n")

        # Step 6.1: Sort eigenvalues and eigenvectors by decreasing eigenvalues
        idxSorted = np.flip(np.argsort(eigenvalues))
        eigenvaluesSorted = eigenvalues[idxSorted]
        variances = eigenvaluesSorted / sum(eigenvaluesSorted)
        eigenvectorsSorted = eigenvectors[:, idxSorted]

        # Step 6.2:
        #  Choose "k" eigenvectors with the largest eigenvalues
        #  Or the "n" eigenvectors that describe more than "k" variance (if k <= 1)
        if self.k <= 1:
            minVariance = self.k
            self.k = next(idx for idx, var in enumerate(np.cumsum(variances)) if var >= self.k) + 1

            # Plot variance
            plotData.plotVariance(variances, minVariance, savePath=self.path,
                                  fileName=self.datasetName+"_variances")

        self.k = int(self.k)

        eigenvaluesSelected = eigenvaluesSorted[:self.k+1]
        eigenvectorsSelected = eigenvectorsSorted[:, :self.k+1]
        rowFeatureVector = eigenvectorsSelected.T
        print("Eigenvalues sorted and selected:")
        print(eigenvaluesSelected, end="\n\n")
        print("Eigenvectors sorted and selected:")
        print(eigenvectorsSelected, end="\n\n")

        # Step 7: Transform
        self.transformedData = np.matmul(rowFeatureVector, rowDataAdjust.T).T

        # Step 8: Plot new subspace
        if self.plotNewSubspace:
            if self.k > 3:
                print("Plotting the 3D subspace as", self.k, "dimensions are not possible", end="\n\n")
            plotData.plot(data=self.transformedData, k=3 if self.k > 3 else self.k,
                          plotCols=None, title='New subspace (ours)', savePath=self.path,
                          fileName=self.datasetName+"_PCA_subspace")

        # Step 9: Reconstruct data set
        RowDataAdjustNew = np.matmul(rowFeatureVector.T, self.transformedData.T).T
        rowDataOriginal = RowDataAdjustNew + OriginalMean
        print("Reconstruction of the original dataset:")
        print(pd.DataFrame(rowDataOriginal, columns=self.dataset.columns))
