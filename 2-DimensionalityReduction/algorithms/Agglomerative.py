import numpy as np
from sklearn.cluster import AgglomerativeClustering


class Agglomerative:

    """
    Args:
    - K (int): number of clusters
    - dataset: Data points or instances.
    - affinity (str): metric used to compute the linkage.
        - 'euclidean'
        - 'l1'
        - 'l2'
        - 'manhattan'
        - 'cosine'
    - linkage: which linkage criterion to use. The linkage criterion determines which distance to use between sets of observation. The algorithm will merge the pairs of cluster that minimize this criterion.
        - 'ward' (default): minimizes the variance of the clusters being merged.
        - 'average': uses the average of the distances of each observation of the two sets.
        - 'complete' or 'maximum': uses the maximum distances between all observations of the two sets.
        - 'single': uses the minimum of the distances between all observations of the two sets.

    """

    def __init__(self, datasetValues, K=2, affinity='euclidean',
                 linkage='ward', validationClasses=None, verbose=False):
        self.n_clusters = K
        self.datasetValues = datasetValues
        self.affinity = affinity
        self.linkage = linkage
        self.clusters = []
        self.verbose = verbose

        self.model = self.execute()

        self.distributions = None
        if validationClasses is not None:
            self.distributions = self.validate(validationClasses)

    # Execute the algorithm
    def execute(self):
        clustering = AgglomerativeClustering(n_clusters=self.n_clusters, affinity=self.affinity,
                                             linkage=self.linkage).fit(self.datasetValues)
        return clustering

    def get_labels(self):
        return self.model.labels_

    def get_distributions(self):
        return self.distributions

    # Compute the class validation that results in a confusion matrix
    def validate(self, validationClasses):
        K = len(np.unique(self.model.labels_))
        distributions = np.zeros((K, K))
        dict_T = {validationClass: idx for idx, validationClass in enumerate(np.unique(validationClasses))}
        for idx, labelPred in enumerate(self.model.labels_):
            if self.verbose: print("Cluster "+str(idx)+":")
            distributions[labelPred][dict_T[validationClasses[idx]]] += 1
            if self.verbose: print("\t"+str(distributions))

        distributions_aux = distributions.copy()
        distributions_ordered = []

        def _coherence_metric(cluster_distribution, position):
            cluster_copy = np.array(cluster_distribution.copy())
            cluster_copy *= -1
            cluster_copy[position] *= -1
            return np.sum(cluster_copy[position:])

        position_count = 0
        while len(distributions_ordered) < K:
            coherences = []
            for distribution in distributions_aux:
                coherences.append(_coherence_metric(distribution, position_count))

            bestClusterIdx = np.argmax(coherences)

            distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
            distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

            if np.squeeze(distributions_aux).ndim == 1:
                distributions_ordered.append(list(distributions_aux[0]))
            position_count += 1

        return distributions_ordered
