import numpy as np


class Cluster:
    def __init__(self, datasetValues, centroid):
        self.datasetValues = datasetValues
        self.original_centroid = centroid
        self.centroid = centroid
        self.samples = []  # indices

    def get_centroid(self):
        return self.centroid

    def get_samples(self):
        return self.samples

    def set_samples(self, samples):
        self.samples = samples

    def recompute_centroid(self):
        if len(self.datasetValues[self.samples]) == 0:
            self.centroid = self.original_centroid
        else:
            self.centroid = np.mean(self.datasetValues[self.samples], axis=0)

    def get_variance(self):
        if len(self.samples) == 0:
            return 0
        return np.sum((abs(self.datasetValues[self.samples] - self.centroid)) ** 2)/len(self.samples)

    def get_samples_distribution(self, validationClasses):
        distribution = {valClass: 0 for valClass in np.unique(validationClasses)}

        for sample in self.samples:
            distribution[validationClasses[sample]] += 1

        return distribution

    def get_overall_similarity(self):

        # Normalize samples
        samples = self.datasetValues[self.samples]
        norms = np.linalg.norm(samples, axis=1)
        norms[norms == 0] = 1
        samplesNorm = (samples.T / norms).T

        # Compute centroid and similarity
        newCentroid = np.mean(samplesNorm, axis=0)
        similarity = np.dot(newCentroid, newCentroid)

        return similarity
