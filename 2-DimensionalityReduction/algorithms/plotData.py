import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder

'''In order to plot and visualize the results we use some instructions extracted from: 
https://www.datacamp.com/community/tutorials/introduction-t-sne which allow a better representation of the results. 
Nevertheless we will specify clearly which part has not been created by us referencing this parts as (NOT OUR)'''

#begin (NOT OUR)
import seaborn as sns
sns.set_style('darkgrid')
sns.set_palette('muted')
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})
#end (NOT OUR)


def plot(data, k, plotCols=None, classesValues=None, title=None, savePath=None, fileName=None):
    """
    Args:
        - data (array): Data points or instances.
        - k (int): The number of dimensions to plot.
        - plotCols (array): Original columns to be plotted, if None components are assumed to be plotted
        - classesValues (array): True classes labels of the original data.
        - title (string): Title of the plot.
        - savePath (string): String that represent a path to save the images, if None, the plots will no be saved.
        - fileName (string): Name of the dataset to use it in the saving plots.
    """

    # Default for variance explained experiments
    if k <= 1:
        k = 3

    # Classes colors
    colors = 'red'
    if classesValues is not None:
        labels = np.unique(classesValues)
        #begin (NOT OUR)
        palette = np.array(sns.color_palette("hls", len(labels)))
        id = LabelEncoder().fit_transform(classesValues.ravel())
        colors = palette[id]
        #end (NOT OUR)

    # 3D plot
    if k > 2:
        fig = plt.figure(figsize=(8,6))
        ax = fig.add_subplot(111, projection='3d', elev=-150, azim=110)
        ax.scatter(data[:, 0], data[:, 1], data[:, 2], c=colors)
        if not plotCols:
            ax.set_xlabel("1st eigenvector")
            ax.w_xaxis.set_ticklabels([])
            ax.set_ylabel("2nd eigenvector")
            ax.w_yaxis.set_ticklabels([])
            ax.set_zlabel("3rd eigenvector")
            ax.w_zaxis.set_ticklabels([])
        else:
            ax.set_xlabel(plotCols[0])
            ax.set_ylabel(plotCols[1])
            ax.set_zlabel(plotCols[2])

    # 2D plot
    else:
        plt.scatter(data[:, 0], data[:, 1], c=colors)
        if not plotCols:
            plt.xlabel("1st eigenvector")
            plt.xticks([])
            plt.ylabel("2nd eigenvector")
            plt.yticks([])
        else:
            plt.xlabel(plotCols[0])
            plt.ylabel(plotCols[1])

    # Title
    if title:
        plt.title(title)

    # Save the plot
    if savePath:
        assert fileName, "filename parameter must be defined when using the savepath"
        check_and_create_folder("../results/")
        check_and_create_folder(savePath)
        plt.savefig(savePath+fileName, dpi=200)
        print("Plot saved correctly at " + savePath+fileName + ".png")

    plt.show()


def plotVariance(variances, minVariance, savePath=None, fileName=None):
    """
    Args:
        - variances (array): Variances (normalized) of each component.
        - minVariance (float): Minimum desired variance.
        - savePath (string): String that represent a path to save the images, if None, the plots will no be saved.
        - fileName (string): Name of the dataset to use it in the saving plots.
    """

    fig = plt.figure(figsize=(8, 6))

    # Accumulative variance
    cumVariances = np.cumsum(variances)

    # Plot individual and accumulated variances
    plt.bar(range(1, len(variances)+1), variances, color='tab:orange',
            align='center', label="Individual explained variance")
    plt.step(range(1, len(variances)+1), cumVariances, color='tab:blue',
             where='mid', label="Accumulated explained variance")
    plt.axhline(y=minVariance, color='tab:red', linestyle='-', label="Minimum explained variance")

    # Selected components
    idx = next(idx for idx, var in enumerate(np.cumsum(variances)) if var >= minVariance) + 1       # plot starts at 1
    plt.plot([idx, idx], [0, cumVariances[idx - 1]], color='tab:green',
             linestyle='-', label="Number of components selected")

    # Configure plot
    plt.title("Explained individual and accumulated variance")
    plt.ylabel('Explained variance (%)')
    plt.xlabel('Component index')
    plt.legend()

    # Save the plot
    if savePath:
        assert fileName, "filename parameter must be defined when using the savepath"
        check_and_create_folder("../results/")
        check_and_create_folder(savePath)
        plt.savefig(savePath+fileName, dpi=200)
        print("Plot saved correctly at " + savePath+fileName + ".png")
    plt.show()


def check_and_create_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)
        folders = path.split("/")
        print("Folder "+folders[-2]+" created.")