import pandas as pd
import sklearn
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

"""
Internal
"""


def calinski_harabasz(dataset, labels):
    return sklearn.metrics.calinski_harabasz_score(dataset, labels)


def silhouette_score(dataset, labels):
    return sklearn.metrics.silhouette_score(dataset, labels)


def davies_bouldin(dataset, labels):
    return sklearn.metrics.davies_bouldin_score(dataset, labels)


"""
External
"""


def accuracy(distributions):
    correct = 0
    total = np.sum(distributions)
    for count, distribution in enumerate(distributions):
        correct += distribution[count]
    return correct/total*100


def confussion_matrix_labels(y_true, y_pred):
    return sklearn.metrics.confusion_matrix(y_true, y_pred)


def accuracy_labels(y_true, y_pred):
    return sklearn.metrics.accuracy_score(y_true, y_pred)


def recall_and_precision(y_true, y_pred, average='micro'):
    precision, recall, fscore, _ = sklearn.metrics.precision_recall_fscore_support(y_true, y_pred, average=average, zero_division=0)
    return recall, precision


def rand_index(labels_true, labels_pred, adjusted=True):
    if adjusted:
        return sklearn.metrics.adjusted_rand_score(labels_true, labels_pred)
    return sklearn.metrics.rand_score(labels_true, labels_pred)


def mutual_info(labels_true, labels_pred):
    return sklearn.metrics.mutual_info_score(labels_true, labels_pred)


def fowlkes_mallows(labels_true, labels_pred):
    return sklearn.metrics.fowlkes_mallows_score(labels_true, labels_pred, sparse=False)


def plot_heatmap(distributions, title, size=(10,10), save=False, path=None):
    sns.set(font_scale=2)
    fig, ax = plt.subplots(figsize=size)
    n_clusters = ["Cluster "+str(value+1) for value in range(len(distributions))]
    dataframe = pd.DataFrame(distributions,columns=n_clusters,index=n_clusters)
    plt.title(title)
    sns.heatmap(dataframe, annot=True, cmap="YlGnBu", square=True, annot_kws={"fontsize":20})
    if save:
        plt.savefig(path, dpi=200)
        print("Heatmap saved correctly at " + path[3:] + ".png")
    plt.show()
    sns.set(font_scale=1)
