import os
import numpy as np
import matplotlib.pyplot as plt
from metrics import validationMetrics
from sklearn.preprocessing import LabelEncoder
from algorithms import Agglomerative, KMeansUB


"""
    Args:
        - datasetName (string): Name of the dataset
        - datasetValues (array): Data points or instances.
        - classes (array): Truth ground classes of the dataset
        - methodValue (int): Clustering method to use. 0 is KMeans and 1 AgglomerativeClustering
        - rangeK (string): String with the range of k for clustering
        - onlyOneK (bool): Indicates that we want to perform the clustering with only one k, the first of the rangeK
        - save (bool): True if we want to save the results
        - options (dict): Dictionary with the selected method options
        - verbose (bool): True if you want verbose information about the process, false otherwise
"""


def applyClustering(datasetName, datasetValues, classes, methodValue, rangeK,
                    save=False, options=None, onlyOneK=False, verbose=True):

    # Get the method name
    methodName = 'KMeans' if methodValue == 0 else 'AGG'

    # Obtain real classes
    labelsTrue = LabelEncoder().fit_transform(classes.values.ravel())
    numRealClasses = len(np.unique(classes))

    # To check the different algorithms (with validation metrics) using the dataset above
    # Creating lists to store the values given by each algorithm
    randIndexs, fowlkes_mallows, calinskis, davies = [], [], [], []

    # Set number of clusters to check
    Krange = [int(val) for val in rangeK.split(" ")]

    # Set N random seeds
    seeds = [7296, 15994, 59377, 6918, 29889, 27115, 17736, 7136, 75369, 20987]

    # If only one K select a random seed and the first k
    if onlyOneK:
        seeds = [np.random.choice(seeds)]
        num_clusters = range(Krange[0], Krange[0] + 1)
    else:
        num_clusters = range(Krange[0], Krange[1] + 1)

    # To store the selected algorithm
    m = None

    # To store the accuracies, distributions and pred_labels
    labels = []
    accuracies = []
    distributions = []

    # Main loop through all the number of clusters
    for it, num_cluster in enumerate(num_clusters):
        print("Number of clusters:", num_cluster)
        randIndexs_aux, fowlkes_mallows_aux, calinskis_aux, silhouettes_aux, davies_aux = [], [], [], [], []

        for seed in seeds:
            print("Seed:", seed)
            if methodName == 'KMeans':  # KMeans execution
                distance, iterations = options["distance"], options["iter"]
                pvalue, initialization = options["p"], options["initialization"]
                if num_cluster != numRealClasses:
                    m = KMeansUB.KMeansUB(K=num_cluster, datasetValues=datasetValues, seed=seed, distance=distance,
                                          p=pvalue, max_iterations=iterations, kmeans_plus_plus=initialization,
                                          verbose=False)
                else:
                    m = KMeansUB.KMeansUB(K=num_cluster, datasetValues=datasetValues, seed=seed, distance=distance,
                                          p=pvalue, max_iterations=iterations, kmeans_plus_plus=initialization,
                                          verbose=False, validationClasses=classes)
                    distribution = m.get_distributions()
                    distributions.append(distribution)
                    accuracies.append(round(validationMetrics.accuracy(distribution), 2))

            elif methodName == 'AGG':
                affinity, linkage = options["affinity"], options["linkage"]
                if num_cluster != numRealClasses:
                    m = Agglomerative.Agglomerative(K=num_cluster, datasetValues=datasetValues,
                                                    affinity=affinity, linkage=linkage)
                else:
                    m = Agglomerative.Agglomerative(K=num_cluster, datasetValues=datasetValues,
                                                    affinity=affinity, linkage=linkage, validationClasses=classes)
                    distribution = m.get_distributions()
                    distributions.append(distribution)
                    accuracies.append(round(validationMetrics.accuracy(distribution), 2))

            labelsPred = m.get_labels()
            assert len(np.unique(labelsPred)) != 1, \
                "The selected parameters collapsed all the samples in the same cluster"

            randIndexs_aux.append(validationMetrics.rand_index(labelsTrue, labelsPred))
            fowlkes_mallows_aux.append(validationMetrics.fowlkes_mallows(labelsTrue, labelsPred))
            calinskis_aux.append(validationMetrics.calinski_harabasz(datasetValues, labelsPred))
            davies_aux.append(validationMetrics.davies_bouldin(datasetValues, labelsPred))

        labels.append(labelsPred)

        randIndexs.append(np.mean(randIndexs_aux))
        fowlkes_mallows.append(np.mean(fowlkes_mallows_aux))
        calinskis.append(np.mean(calinskis_aux))
        davies.append(np.mean(davies_aux))

    if verbose:
        if not onlyOneK:
            plot_figures(accuracies, randIndexs, fowlkes_mallows, calinskis, davies,
                         datasetName=datasetName, save=save, methodValue=methodValue,
                         rangeK=rangeK, distributions=distributions, classes=classes)
        else:
            print("Adjusted rand index =", np.round(randIndexs, 4))
            print("Fowlkes Mallows =", np.round(fowlkes_mallows, 4))
            print("Calinski-Harabasz score =", np.round(calinskis, 4))
            print("Davies Bouldin =", np.round(davies, 4))

    return accuracies, (randIndexs, fowlkes_mallows, calinskis, davies), distributions, labels


"""
Plot figures for the clustering methods
"""


def plot_figures(accuracies, randIndexs, fowlkes_mallows, calinskis, davies, datasetName=None,
                 save=False, methodValue=None, rangeK=None, distributions=None, classes=None):

    # Set number of clusters to check
    rangeK = [int(val) for val in rangeK.split(" ")]
    num_clusters = range(rangeK[0], rangeK[1] + 1)

    # Get method and classes
    methodName = 'KMeans' if methodValue == 0 else 'AGG'
    numRealClasses = len(np.unique(classes))

    # Get metrics normalized
    randIndexs_norm = randIndexs/np.max(randIndexs)
    fowlkes_mallows_norm = fowlkes_mallows/np.max(fowlkes_mallows)
    calinskis_norm = calinskis/np.max(calinskis)
    davies_norm = davies/np.max(davies)

    # Plot all the metrics
    idxRandIndexs = np.argmax(randIndexs)
    if np.max(randIndexs) < 0:
        randIndexs_norm = -randIndexs_norm
        print("Maximum rand index is below 0")

    plt.figure(1)
    plt.plot(num_clusters, randIndexs_norm, '.-', color='red', label="Adjusted rand index (max)")
    plt.scatter(num_clusters[idxRandIndexs], randIndexs_norm[idxRandIndexs], c='red', marker='*')

    idxFowlkes = np.argmax(fowlkes_mallows)
    plt.plot(num_clusters, fowlkes_mallows_norm, '.-', color='blue', label="Fowlkes Mallows (max)")
    plt.scatter(num_clusters[idxFowlkes], fowlkes_mallows_norm[idxFowlkes], c='blue', marker='*')

    idxCalinski = np.argmax(calinskis)
    plt.plot(num_clusters, calinskis_norm, '.-', color='green', label="Calinski-Harabasz score (max)")
    plt.scatter(num_clusters[idxCalinski], calinskis_norm[idxCalinski], c='green', marker='*')

    idxDavies = np.argmin(davies)
    plt.plot(num_clusters, davies_norm, '.-', color='orange', label="Davies Bouldin (min)")
    plt.scatter(num_clusters[idxDavies], davies_norm[idxDavies], c='orange', marker='*')

    plt.legend()
    plt.title("Comparison number of clusters of " + str(datasetName) + " dataset with method " + str(methodName) +
              ".\nAverage accuracy (among all seeds): " + str(round(np.mean(accuracies), 2)) + "%")

    if save:
        check_and_create_folder("../results/")
        check_and_create_folder("../results/" + str(datasetName) + "/")
        path = "../results/" + str(datasetName) + "/" + str(methodName) + "-" + str(rangeK[0]) + "_" + str(
            rangeK[1])
        plt.savefig(path, dpi=200)
        print("Plot saved correctly at " + path[3:] + ".png")
    plt.show()

    # Plot heatmap
    idx = np.argmax(accuracies)
    validationMetrics.plot_heatmap(distributions[idx], size=(15, 15), save=save,
                                   path="../results/"+str(datasetName)+"/"+str(methodName)+"-Matrix_"+str(numRealClasses),
                                   title="Best confussion matrix for " + str(datasetName) + " dataset with method " +
                                         str(methodName) + "\nBest accuracy: " + str(round(accuracies[idx], 2)) + "%")

    # Show the best metrics and accuracy
    print("Adjusted rand index =", np.round(randIndexs, 4))
    print("Fowlkes Mallows =", np.round(fowlkes_mallows, 4))
    print("Calinski-Harabasz score =", np.round(calinskis, 4))
    print("Davies Bouldin =", np.round(davies, 4))
    print("Best accuracy =", round(accuracies[idx], 4), ". Mean accuracy =", round(np.mean(accuracies), 4))


def check_and_create_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)
        folders = path.split("/")
        print("Folder "+folders[-2]+" created.")
